include("shared.lua")

billboard_images = {}

file.CreateDir("billboard_data")

local function saveImage(data, id)
	file.Write( "billboard_data/"..id..".png", data )
end

local function cacheImage(id)
	billboard_images[id] = "billboard_data/"..id..".png"
end

local function checkImage(id)
	if file.Exists( "billboard_data/"..id..".png", "DATA" ) then
		return true
	else
		return false
	end
end

local function downloadImage(id)
	if checkImage(id) then return end
	local userURL = "https://i.imgur.com/"..id..".png"
	http.Fetch( userURL,
		function( body, len, headers, code )
			saveImage(body, id)
			cacheImage(id)
		end
	)
end

// Default Background
downloadImage("4rlZsQn")

// Image Icons
downloadImage("dEAJfJB") // Burger
downloadImage("mo9rQxm") // Crowbar
downloadImage("dyM8Pxt") // Cuffs
downloadImage("y7mxfXB") // Gun
downloadImage("fo2BhqF") // Heart
downloadImage("vsNdsxe") // Pizza
downloadImage("qTCTQ18") // Shield

surface.CreateFont( "PlainFont", {
	font = "Roboto",
	size = 80,
	antialias = true
} )

surface.CreateFont( "CurvyFont", {
	font = "Courier New",
	size = 80,
	antialias = true
} )

surface.CreateFont( "SuperFont", {
	font = "Helvetica",
	size = 80,
	antialias = true
} )

function ENT:Initialize()

	self:SetNetworkedString("ChosenMaterial", "dEAJfJB")
	self:SetNetworkedString("ImageID", "4rlZsQn")
	self:SetNetworkedString("ImageString", "dEAJfJB")

	self:SetNetworkedString("TextLineOne", "The First Line")
	self:SetNetworkedString("TextLineTwo", "The Second Line")
	self:SetNetworkedString("TextFont", "PlainFont")

	self:SetNetworkedVector("TextColor", Color(255,0,0))

	self:SetNetworkedBool("ShowTextOne", true)
	self:SetNetworkedBool("ShowTextTwo", true)
	self:SetNetworkedBool("ShowImage", true)

end

function ENT:Draw()

	local camPos = self:LocalToWorld( Vector( 52, 3, 29 ) )
	local camAng = self:LocalToWorldAngles( Angle( 0, 180, 90 ) )

	self:DrawModel()

	cam.Start3D2D( camPos , camAng , 0.1)

		local frameWidth = 1040
		local frameHeight = 575

		// Main Box
		draw.RoundedBox(0,0,0,frameWidth,frameHeight,Color(0,0,0))

		if self:GetPos():Distance(LocalPlayer():GetPos()) < 500 then

			if !billboard_images[self:GetNetworkedString("ImageID")] then
				downloadImage(self:GetNetworkedString("ImageID"))
			end

		    surface.SetDrawColor(Color(255,255,255))
		    surface.SetMaterial(Material("data/billboard_data/"..self:GetNetworkedString("ImageID")..".png"))
		    surface.DrawTexturedRect(0, 0, frameWidth, frameHeight)

		    if self:GetNetworkedBool("ShowTextOne") then
				draw.SimpleTextOutlined(self:GetNetworkedString("TextLineOne"),self:GetNetworkedString("TextFont"),30,300,self:GetNetworkedVector("TextColor"),TEXT_ALIGN_LEFT,TEXT_ALIGN_TOP,2,Color(0,0,0))
			end

			if self:GetNetworkedBool("ShowTextTwo") then
				draw.SimpleTextOutlined(self:GetNetworkedString("TextLineTwo"),self:GetNetworkedString("TextFont"),150,400,self:GetNetworkedVector("TextColor"),TEXT_ALIGN_LEFT,TEXT_ALIGN_TOP,2,Color(0,0,0))	
			end

			if self:GetNetworkedBool("ShowImage") then
		    	surface.SetDrawColor(color_white)
		    	surface.SetMaterial( Material("data/billboard_data/"..self:GetNetworkedString("ChosenMaterial")..".png") )
		    	surface.DrawTexturedRect(760, 20, 250, 250)    
		    end

		end

	cam.End3D2D()	

end

local white = Color(255,255,255)
local darkblue = Color(3,20,36)
local blue = Color(48,65,93)
local lightblue = Color(142,174,189)
local red = Color(207,103,102)

net.Receive("butts_billboard_small_open",function()

	local entity = net.ReadEntity()

	local mainWidth = 300
	local mainHeight = 280
	
	--[[ Main Frame ]]--

	local frame = vgui.Create("DFrame")
	frame:SetSize(mainWidth,mainHeight)
	frame:SetTitle("Billboard Editor")
	frame:Center()
	frame:ShowCloseButton(true)
	frame:SetVisible(true)
	frame:MakePopup()

	local panel = vgui.Create( "DPanel", frame )
	panel:SetPos( 5, 30 )
	panel:SetSize( mainWidth - 10, mainHeight - 35 )
	panel.Paint = function()
		draw.RoundedBox( 8, 0, 0, panel:GetWide(), panel:GetTall(), Color( 108,111,114 ) )
	end

	local sheet = vgui.Create( "DPropertySheet", panel )
	sheet:SetPadding(0)
	sheet:Dock( FILL )
	sheet.Paint = function()
		draw.RoundedBox( 8, 0, 0, sheet:GetWide(), sheet:GetTall(), Color( 108,111,114 ) )
	end

	--[[

		Options Panel

	]]

	local optionPanel = vgui.Create( "DPanel", sheet )
	optionPanel.Paint = function()
		draw.RoundedBox( 8, 0, 0, optionPanel:GetWide(), optionPanel:GetTall(), Color( 108,111,114 ) )

		draw.SimpleText("Display First Line of Text?","DermaDefault",mainWidth / 4,20,Color(210, 210, 210),TEXT_ALIGN_LEFT,TEXT_ALIGN_TOP)
		draw.SimpleText("Display Second Line of Text?","DermaDefault",mainWidth / 4,60,Color(210, 210, 210),TEXT_ALIGN_LEFT,TEXT_ALIGN_TOP)
		draw.SimpleText("Display Image?","DermaDefault",mainWidth / 4,100,Color(210, 210, 210),TEXT_ALIGN_LEFT,TEXT_ALIGN_TOP)
	end	

	sheet:AddSheet("Options", optionPanel)

	local textOneCheckBox = vgui.Create("DCheckBox", optionPanel)
	textOneCheckBox:SetPos(mainWidth/4, 40)
	textOneCheckBox:SetValue(entity:GetNetworkedBool("ShowTextOne"))

	local textTwoCheckBox = vgui.Create("DCheckBox", optionPanel)
	textTwoCheckBox:SetPos(mainWidth/4, 80)
	textTwoCheckBox:SetValue(entity:GetNetworkedBool("ShowTextTwo"))	

	local imageCheckBox = vgui.Create("DCheckBox", optionPanel)
	imageCheckBox:SetPos(mainWidth/4, 120)
	imageCheckBox:SetValue(entity:GetNetworkedBool("ShowImage"))		

	// Submit Button
	local optionButton = vgui.Create("DButton", optionPanel)
	optionButton:SetText("Update Billboard")
	optionButton:SetPos(mainWidth / 4, 195)
	optionButton:SetSize(mainWidth / 2, 25)
	optionButton.DoClick = function()

		net.Start("billboard_send_options")

			net.WriteBool( textOneCheckBox:GetChecked() )
			net.WriteBool( textTwoCheckBox:GetChecked() )
			net.WriteBool( imageCheckBox:GetChecked() )
			net.WriteEntity( entity )

		net.SendToServer()

		frame:Close()
	end		

	--[[

		Naming Panel

	]]	

	local passivePanel = vgui.Create( "DPanel", sheet )
	passivePanel.Paint = function()
		draw.RoundedBox( 8, 0, 0, passivePanel:GetWide(), passivePanel:GetTall(), Color( 108,111,114 ) )

		draw.SimpleText("First Line  (max 25 char):","DermaDefault",mainWidth / 4,12,Color(210, 210, 210),TEXT_ALIGN_LEFT,TEXT_ALIGN_TOP)
		draw.SimpleText("Second Line  (max 25 char):","DermaDefault",mainWidth / 4,62,Color(210, 210, 210),TEXT_ALIGN_LEFT,TEXT_ALIGN_TOP)
	end	

	sheet:AddSheet("Text", passivePanel)

	// First Input:

	local firstText = vgui.Create( "DTextEntry", passivePanel ) -- create the form as a child of frame
	firstText:SetPos( mainWidth / 4, 30 )
	firstText:SetSize( mainWidth / 2, 25 )
	firstText:SetText( entity:GetNetworkedString("TextLineOne") )

	// Second Input:

	local secondText = vgui.Create( "DTextEntry", passivePanel ) -- create the form as a child of frame
	secondText:SetPos( mainWidth / 4, 80 )
	secondText:SetSize( mainWidth / 2, 25 )
	secondText:SetText( entity:GetNetworkedString("TextLineTwo") )

	// Font Input:

	local label = vgui.Create("DLabel",passivePanel)
	label:SetPos(mainWidth / 4, 105)
	label:SetText("Font:")

	local cbox = vgui.Create( "DComboBox", passivePanel )
	cbox:SetPos(mainWidth / 4, 125)
	cbox:SetSize( mainWidth / 2, 25 )
	cbox:SetSortItems(false)

	cbox:SetValue( "Choose a font" )
	cbox:AddChoice( "PlainFont" )
	cbox:AddChoice( "CurvyFont" )
	cbox:AddChoice( "SuperFont" )	

	local colorShapeBlack = vgui.Create("DShape", passivePanel)
	colorShapeBlack:SetType("Rect")
	colorShapeBlack:SetColor(Color(0,0,0))
	colorShapeBlack:SetPos(((mainWidth / 4) * 3) + 5, 155)
	colorShapeBlack:SetSize(40, 40)

	local colorShape = vgui.Create("DShape", passivePanel)
	colorShape:SetType("Rect")
	colorShape:SetColor(entity:GetNetworkedVector("TextColor"))
	colorShape:SetPos(((mainWidth / 4) * 3) + 7, 157)
	colorShape:SetSize(36, 36)

	local colorPalette = vgui.Create("DColorPalette", passivePanel)
	colorPalette:SetPos(mainWidth / 4, 155)
	colorPalette:SetSize(mainWidth / 2, 40 )
	colorPalette.OnValueChanged = function( s, value )
		colorShape:SetColor( value )
	end

	// Submit Button
	local nameButton = vgui.Create("DButton", passivePanel)
	nameButton:SetText("Update Billboard")
	nameButton:SetPos(mainWidth / 4, 200)
	nameButton:SetSize(mainWidth / 2, 25)
	nameButton.DoClick = function()

		if string.len(firstText:GetValue()) > 25 then return end
		if string.len(secondText:GetValue()) > 25 then return end
		if !cbox:GetSelected() then return end

		net.Start("billboard_send_text")

			local colTable = colorShape:GetColor()
			local color = Vector( colTable.r , colTable.g , colTable.b )

			net.WriteString( firstText:GetValue() )
			net.WriteString( secondText:GetValue() )
			net.WriteString( cbox:GetSelected() )
			net.WriteVector( color )
			net.WriteEntity( entity )

		net.SendToServer()

		frame:Close()
	end	

	--[[ 

		Image Panel

	]]

	local matTable = {
		["Burger"] = "dEAJfJB",
		["Crowbar"] = "mo9rQxm",
		["Cuffs"] = "dyM8Pxt",
		["Gun"] = "y7mxfXB",
		["Heart"] = "fo2BhqF",
		["Pizza"] = "vsNdsxe",
		["Shield"] = "qTCTQ18"
	}

	local imagePanel = vgui.Create( "DPanel", sheet )
	imagePanel.Paint = function()
		draw.RoundedBox( 8, 0, 0, imagePanel:GetWide(), imagePanel:GetTall(), Color( 108,111,114 ) )
	end		
	sheet:AddSheet("Image", imagePanel)

	local image = vgui.Create("DImage",imagePanel)
	image:SetPos( mainWidth / 4, 5 )
	image:SetSize( 150, 150 )
	image:SetImage("data/billboard_data/"..entity:GetNetworkedString("ImageString")..".png")

	local imgBox = vgui.Create( "DComboBox", imagePanel )
	imgBox:SetPos(mainWidth / 4, 160)
	imgBox:SetSize( mainWidth / 2, 25 )
	imgBox:SetSortItems(false)

	imgBox:SetValue( "Choose an image" )
	imgBox:AddChoice( "Burger" )
	imgBox:AddChoice( "Crowbar" )
	imgBox:AddChoice( "Cuffs" )
	imgBox:AddChoice( "Gun" )
	imgBox:AddChoice( "Heart" )
	imgBox:AddChoice( "Pizza" )	
	imgBox:AddChoice( "Shield" )

	imgBox.OnSelect = function()
		local choice, data = imgBox:GetSelected()
		if choice == "" then return end
		entity:SetNetworkedString("ImageString", matTable[choice])
		image:SetImage("data/billboard_data/"..matTable[choice]..".png")
	end	

	// Submit Button
	local imgButton = vgui.Create("DButton", imagePanel)
	imgButton:SetText("Update Billboard")
	imgButton:SetPos(mainWidth / 4, 195)
	imgButton:SetSize(mainWidth / 2, 25)
	imgButton.DoClick = function()
		local choice, data = imgBox:GetSelected()
		if choice == "" then return end

		net.Start("billboard_send_material")

			net.WriteString( matTable[choice] )
			net.WriteEntity( entity )

		net.SendToServer()

		frame:Close()
	end	

    --[[ 

		Background Image Panel

	]]

	local bgImagePanel = vgui.Create( "DPanel", sheet )
	bgImagePanel.Paint = function()
		draw.RoundedBox( 8, 0, 0, bgImagePanel:GetWide(), bgImagePanel:GetTall(), Color( 108,111,114 ) )
		draw.SimpleText("Imgur ID for background image:","DermaDefault",mainWidth / 4,60,Color(210, 210, 210),TEXT_ALIGN_LEFT,TEXT_ALIGN_TOP)
		draw.SimpleText("Resolution: 1113x580","DermaDefault",mainWidth / 4,120,Color(210, 210, 210),TEXT_ALIGN_LEFT,TEXT_ALIGN_TOP)
	end		
	sheet:AddSheet("Background Image", bgImagePanel)	

	local bgImageText = vgui.Create( "DTextEntry", bgImagePanel ) -- create the form as a child of frame
	bgImageText:SetPos( mainWidth / 4, 80 )
	bgImageText:SetSize( mainWidth / 2, 30 )
	bgImageText:SetText( "ImgurID" )	

	// Submit Button
	local bgImgButton = vgui.Create("DButton", bgImagePanel)
	bgImgButton:SetText("Update Billboard")
	bgImgButton:SetPos(mainWidth / 4, 195)
	bgImgButton:SetSize(mainWidth / 2, 25)
	bgImgButton.DoClick = function()
		if bgImageText then	
			downloadImage(bgImageText:GetValue())

			net.Start("billboard_send_image")

				net.WriteString( bgImageText:GetValue() )
				net.WriteEntity( entity )

			net.SendToServer()

			frame:Close()
		end
	end		

end)
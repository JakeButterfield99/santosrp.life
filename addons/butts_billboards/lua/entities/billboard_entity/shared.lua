ENT.Type = "anim"
ENT.Base = "base_gmodentity"

ENT.PrintName = "Billboard - Test"

ENT.Spawnable = true

function ENT:SetupDataTables()

	self:NetworkVar("String",0,"ImageID")
	self:NetworkVar("String",1,"ChosenMaterial")
	self:NetworkVar("String",2,"ImageString")

	self:NetworkVar("String",3,"TextLineOne")
	self:NetworkVar("String",4,"TextLineTwo")
	self:NetworkVar("String",5,"TextFont")

	self:NetworkVar("Vector",0,"TextColor")

	self:NetworkVar("Bool",0,"ShowTextOne")
	self:NetworkVar("Bool",1,"ShowTextTwo")
	self:NetworkVar("Bool",2,"ShowImage")

end
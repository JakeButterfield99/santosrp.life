AddCSLuaFile("cl_init.lua")
AddCSLuaFile("shared.lua")

include("shared.lua")

util.AddNetworkString("butts_billboard_small_open")
util.AddNetworkString("billboard_send_options")
util.AddNetworkString("billboard_send_text")
util.AddNetworkString("billboard_send_material")
util.AddNetworkString("billboard_send_image")

function ENT.Initialize(self)

	self:SetModel("models/devonjones/santos/storesign.mdl")
	self:SetUseType(SIMPLE_USE)
	self:PhysicsInit(SOLID_VPHYSICS)
	self:SetMoveType(MOVETYPE_VPHYSICS)
	self:SetSolid(SOLID_VPHYSICS)
	//self:SetModelScale(0.5)

	local phys = self:GetPhysicsObject()

	if phys:IsValid() then

		phys:Wake()

	end

end

function ENT:SpawnFunction(ply,tr,class)

	if ( !tr.Hit ) then return end

	local SpawnPos = tr.HitPos + tr.HitNormal * 40
	local SpawnAng = ply:EyeAngles()
	SpawnAng.p = 0
	SpawnAng.y = SpawnAng.y + 180

	local ent = ents.Create( ClassName )
	ent:SetPos( SpawnPos )
	ent:SetAngles( SpawnAng )
	ent:Spawn()
	ent:Activate()

	return ent	

end

function ENT:Use(ply)

	net.Start("butts_billboard_small_open")
		net.WriteEntity(self)
	net.Send(ply)

end

net.Receive("billboard_send_options",function()

	local show1 = net.ReadBool()
	local show2 = net.ReadBool()
	local showImage = net.ReadBool()

	local entity = net.ReadEntity()

	if !IsValid( entity ) then return end

	entity:SetNetworkedBool("ShowTextOne", show1 )
	entity:SetNetworkedBool("ShowTextTwo", show2 )
	entity:SetNetworkedBool("ShowImage", showImage )

end)

net.Receive("billboard_send_text",function()

	local text1 = net.ReadString()
	local text2 = net.ReadString()
	local font = net.ReadString()
	local color = net.ReadVector()

	local entity = net.ReadEntity()

	if !IsValid( entity ) then return end

	entity:SetNetworkedString("TextLineOne", text1 )
	entity:SetNetworkedString("TextLineTwo", text2 )
	entity:SetNetworkedString("TextFont", font )
	entity:SetNetworkedVector("TextColor", color )

end)

net.Receive("billboard_send_material",function()

	local mat = net.ReadString()

	local entity = net.ReadEntity()

	if !IsValid( entity ) then return end

	entity:SetNetworkedString("ChosenMaterial", mat )	

end)

net.Receive("billboard_send_image",function()

	local image = net.ReadString()

	local entity = net.ReadEntity()

	if !IsValid( entity ) then return end

	entity:SetNetworkedString("ImageID", image )

end)
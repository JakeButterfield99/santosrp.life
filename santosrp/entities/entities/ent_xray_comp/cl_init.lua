include("shared.lua")

surface.CreateFont( "CompText", {
	font = "Tahoma",
	antialias = true,
	size = 12,
} )

local damageTable = {
  ["Head"]    = "PHead",
  ["Chest"]     = "PChest",
  ["Left Arm"]  = "PLArm",
  ["Right Arm"]   = "PRArm",
  ["Left Leg"]  = "PLLeg",
  ["Right Leg"]   = "PLReg",
}

function ENT:Draw()

  	local camPos = self:LocalToWorld( Vector( 10.1, -8.8, 22.4 ) )
  	local camAng = self:LocalToWorldAngles( Angle( 0, 90, 82 ) )

  	local patientText = "Patient:"

  	if !(self:GetPlayerName() == "") then
  		patientText	= "Patient: "..self:GetPlayerName()
  	end

  	local resText = ""
  	local showProgress = false
    local showResult = false

  	if self:GetIsScanning() then
  		resText = "Scanning:"
  		showProgress = true
  	elseif self:GetHasResult() then
  		resText = "Result:"
  		showProgress = false
      showResult = true
  	end

  	self:DrawModel()

  	cam.Start3D2D( camPos , camAng , 0.1)

        draw.RoundedBox(0,0,0,174,144,Color(0,0,0))
        draw.RoundedBox(0,0,23,174,1,Color(255,255,255))
        draw.SimpleText("OCFR X-Ray Machine","DermaDefault",87,10,Color(255,255,255),TEXT_ALIGN_CENTER,TEXT_ALIGN_CENTER)

        if !self:GetHasResult() or !self:GetIsScanning() then

	        draw.SimpleText(patientText,"CompText",87,42,Color(255,255,255),TEXT_ALIGN_CENTER,TEXT_ALIGN_CENTER)

	        draw.SimpleText(resText,"CompText",87,65,Color(255,255,255),TEXT_ALIGN_CENTER,TEXT_ALIGN_CENTER)

	        if showProgress then
		        draw.RoundedBox(0,37,73,102,15,Color(169,169,169))
		        draw.RoundedBox(0,38,74,self:GetProgress()/3.6,13,Color(255,0,0))
		      end

          local yPos = 73

          if showResult then

            for k, v in pairs( damageTable ) do

              local txt = self:GetNWBool( v ) and "Broken" or "Healthy"

              txt = k .. ": " .. txt

              draw.SimpleText(txt,"CompText",87,yPos,Color(255,255,255),TEXT_ALIGN_CENTER,TEXT_ALIGN_CENTER)

              yPos = yPos + 10

            end

          end

    		else
    			draw.SimpleText("Start Scanning","DermaDefault",87,75,Color(255,255,255),TEXT_ALIGN_CENTER,TEXT_ALIGN_CENTER)
    		end

  	cam.End3D2D()	


end
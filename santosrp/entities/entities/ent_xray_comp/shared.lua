ENT.Type = "anim"
ENT.Base = "base_gmodentity"

ENT.PrintName = "X-Ray Computer"

ENT.Spawnable = true

function ENT:SetupDataTables()

	self:NetworkVar("Bool",0,"IsScanning")
	self:NetworkVar("Bool",1,"HasResult")

	self:NetworkVar("String",0,"PlayerName")

	self:NetworkVar("Bool",2,"PHead")
	self:NetworkVar("Bool",3,"PChest")
	self:NetworkVar("Bool",4,"PLArm")
	self:NetworkVar("Bool",5,"PRArm")
	self:NetworkVar("Bool",6,"PLLeg")
	self:NetworkVar("Bool",7,"PLReg")

	self:NetworkVar("Int",0,"Progress")

end
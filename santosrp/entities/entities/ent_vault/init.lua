--[[
	Name: init.lua
	For: SantosRP
	By: TalosLife
]]--

local policelimit = 3
local timer = "Loading"

AddCSLuaFile "cl_init.lua"
AddCSLuaFile "shared.lua"
include "shared.lua"

function ENT:Initialize()
	self:SetUseType( SIMPLE_USE )
	self:SetModel( "models/custom/vault.mdl" )
	self:PhysicsInit( SOLID_VPHYSICS )
	self:SetMoveType( MOVETYPE_VPHYSICS )
	self:SetSolid( SOLID_VPHYSICS )
end

function ENT:Think()

    if !self.LastRobbery then self.LastRobbery = 0 end

	if( GAMEMODE.Jobs:GetNumPlayers( JOB_POLICE ) < policelimit )then
		if(tostring(timer) != "Bank Lockdown")then
		
		timer = "Bank Lockdown"
		self:SetNWString("InputVault",timer)

		
		end

	return end

	local minutesbetween = math.Round(CurTime() - self.LastRobbery)
	if(GAMEMODE.Config.BankCoolDown < minutesbetween)then
	
		if(timer != "Bank Active")then
		timer = "Bank Active"
		self:SetNWString("InputVault",timer)
		end

	else
	
	local timeleft = "Not Active"
	
		if(timer != tostring(timeleft))then
			timer = tostring(timeleft)
			self:SetNWString("InputVault",timer)
			
		end
	

	end
	
end

function ENT:OnTakeDamage(info)

if( GAMEMODE.Jobs:GetNumPlayers( JOB_POLICE ) >= policelimit )then
	local minutesbetween = CurTime() - self.LastRobbery
		if(GAMEMODE.Config.BankCoolDown < minutesbetween)then
			if(info:IsExplosionDamage())then
			    
				self.LastRobbery = CurTime()
				
            	strText = "There has been an explosion at the bank!"
            	for k, v in pairs( player.GetAll() ) do
            		if not GAMEMODE.Jobs:GetPlayerJob( v ) then continue end
            		if GAMEMODE.Jobs:GetPlayerJob( v ).Receives911Messages then
            			GAMEMODE.Net:SendTextMessage( v, "Dispatch", strText )
            			v:EmitSound( "taloslife/sms.mp3" )
            		end
            	end				
				
				table.foreach( GAMEMODE.Config.BagLocations, function( key, value )
					local vault = ents.Create( "ent_moneybag" )
					vault:SetPos(  value  )
					vault:SetModel("models/custom/vault.mdl")
					vault.IsItem = true
					vault.ItemID = "Money Bag"
					vault.ItemData = GAMEMODE.Inv:GetItem("Money Bag")
					vault:Spawn()
				end )
			end
		end
	end
	return false
end
AddCSLuaFile("cl_init.lua")
AddCSLuaFile("shared.lua")

include("shared.lua")

local burnerBtnData = { mdl = 'models/maxofs2d/button_02.mdl', pos = Vector('27 25 44'), ang = Angle('-0.000 90.000 90.000') }
local burnerBtnScale = 0.2

function ENT.Initialize(self)

	self:SetModel("models/freeman/money_printer.mdl")
	self:SetUseType(SIMPLE_USE)
	self:PhysicsInit(SOLID_VPHYSICS)
	self:SetMoveType(MOVETYPE_VPHYSICS)
	self:SetSolid(SOLID_VPHYSICS)
	self:SetTrigger( true )

	self:SetSkin( 1 )

	self.NextPress = 0

	self.m_entBtnBurner = ents.Create( "ent_button" )
	self.m_entBtnBurner:SetModel( burnerBtnData.mdl )
	self.m_entBtnBurner:SetPos( self:LocalToWorld(burnerBtnData.pos) )
	self.m_entBtnBurner:SetAngles( self:LocalToWorldAngles(burnerBtnData.ang) )
	self.m_entBtnBurner:Spawn()
	self.m_entBtnBurner:Activate()
	self.m_entBtnBurner:SetCollisionGroup( COLLISION_GROUP_WEAPON )
	self.m_entBtnBurner:SetModelScale( burnerBtnScale )
	self.m_entBtnBurner:SetIsToggle( true )
	self.m_entBtnBurner:SetParent( self )
	self.m_entBtnBurner.BlockPhysGun = true
	self:DeleteOnRemove( self.m_entBtnBurner )

	self.m_entBtnBurner:SetCallback( function( _, pPlayer, bToggle )
		if bToggle then
			if self:GetFuelAmount() > 0 then
				self.sound = CreateSound(self, Sound("ambient/levels/labs/equipment_printer_loop1.wav"))
				self.sound:SetSoundLevel(52)
				self.sound:PlayEx(1, 100)		
				self:SetPrinterOn( bToggle )
				self:SetSkin( 0 )
			end
		else
			self:PowerOff()
		end
	end )	

	local phys = self:GetPhysicsObject()

	if phys:IsValid() then
		phys:Wake()
	end

end

function ENT:PowerOff()

	if self.sound then self.sound:Stop() end
	self:SetSkin( 1 )
	self:SetPrinterOn( false )

end

function ENT:TickNeededItems()

	self:SetFuelAmount( self:GetFuelAmount() - 1 )
	self:SetInkAmount( self:GetInkAmount() - 1 )
	self:SetPaperAmount( self:GetPaperAmount() - 1 )

end

function ENT:Think()

	if !(self:GetPrinterOn()) then self:PowerOff() return end
	if !(self:GetFuelAmount() > 0) then self:PowerOff() return end
	if !(self:GetInkAmount() > 0) then self:PowerOff() return end
	if !(self:GetPaperAmount() > 0) then self:PowerOff() return end
	if !(self:GetPressAttached()) then self:PowerOff() return end

	self:TickNeededItems()

	self:GetChildren()[1]:SetPressedPaperAmount( math.Clamp( self:GetChildren()[1]:GetPressedPaperAmount() + 10 , 0 , 100 ) )

	self:NextThink( CurTime() + 5 )
	return true

end

function ENT:OnRemove()

	if self.sound then self.sound:Stop() end

	if self:GetChildren()[1] then

		self:GetChildren()[1]:SetPos( self:GetChildren()[1]:GetPos() + Vector(0,0,20) )
		self:GetChildren()[1]:SetParent( nil )

	end

end

function ENT:PhysicsCollide( tblData, entOther )

	entOther = tblData.HitEntity

	if !entOther.IsItem then return end

	if entOther.ItemID == "Ink Vial" then

		if self:GetInkAmount() == 100 then return end
		entOther:Remove()
		self:SetInkAmount( math.Clamp( self:GetInkAmount() + 10 , 0 , 100 ) )

	elseif entOther.ItemID == "Cotton" then

		if self:GetPaperAmount() == 100 then return end
		entOther:Remove()
		self:SetPaperAmount( math.Clamp( self:GetPaperAmount() + 10 , 0 , 100 ) )

	elseif entOther:GetClass() == "ent_fuelcan" then

		if self:GetFuelAmount() == 100 then return end
		entOther:Remove()
		self:EmitSound( "ambient/water/water_spray1.wav", 70, 70 )
		self:SetFuelAmount( math.Clamp( self:GetFuelAmount() + 10 , 0 , 100 ) )

	elseif entOther.ItemID == "Printing Press" then

		if self:GetPressAttached() then return end
		if CurTime() < self.NextPress then return end
		entOther:SetPos( self:LocalToWorld( Vector( 0, -48, 35 ) ) )
		entOther:SetAngles( self:LocalToWorldAngles( Angle( 100, 90 , 180 ) ) )
		entOther:SetParent( self )
		self:SetPressAttached( true )

	end


end

function ENT:Use()

end
include("shared.lua")

function ENT:Draw()

	local camPos = self:LocalToWorld( Vector( 30.1, -1.4, 51.7 ) )

	local camAng = self:LocalToWorldAngles( Angle( 0, 90, 90 ) )

	self:DrawModel()

	cam.Start3D2D( camPos , camAng , 0.1)

		local w = 180
		local h = 117

		local on = (self:GetPrinterOn() and "On" or "Off")

		// Main Box
		draw.RoundedBox(0,0,0,w,h,Color(21,27,31,255))
		draw.SimpleText("Printer","DermaDefaultBold",w/2,10,color_white,1,1)
		draw.SimpleText("Current State: "..on,"DermaDefault",w/2,25,color_white,1,1)

		// Fuel Box
		draw.RoundedBox(0,w/2-50,50,100,15,Color(105,105,105,255))
		draw.RoundedBox(0,w/2-50,50,self:GetFuelAmount(),15,Color(255,0,0,255))
		draw.SimpleText("Fuel: "..self:GetFuelAmount().."/100","DermaDefault",w/2,57.5,color_white,1,1)

		// Fuel Box
		draw.RoundedBox(0,w/2-50,70,100,15,Color(105,105,105,255))
		draw.RoundedBox(0,w/2-50,70,self:GetPaperAmount(),15,Color(255,0,0,255))
		draw.SimpleText("Paper: "..self:GetPaperAmount().."/100","DermaDefault",w/2,77.5,color_white,1,1)

		// Fuel Box
		draw.RoundedBox(0,w/2-50,90,100,15,Color(105,105,105,255))
		draw.RoundedBox(0,w/2-50,90,self:GetInkAmount(),15,Color(255,0,0,255))
		draw.SimpleText("Ink: "..self:GetInkAmount().."/100","DermaDefault",w/2,97.5,color_white,1,1)

	cam.End3D2D()	

end
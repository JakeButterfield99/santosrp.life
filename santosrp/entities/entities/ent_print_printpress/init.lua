AddCSLuaFile("cl_init.lua")
AddCSLuaFile("shared.lua")

include("shared.lua")

function ENT.Initialize(self)

	self:SetModel("models/props/de_nuke/NuclearFuelContainer.mdl")
	self:SetUseType(SIMPLE_USE)
	self:PhysicsInit(SOLID_VPHYSICS)
	self:SetMoveType(MOVETYPE_VPHYSICS)
	self:SetSolid(SOLID_VPHYSICS)

	self:SetModelScale( 0.1 )

	local phys = self:GetPhysicsObject()

	if phys:IsValid() then
		phys:Wake()
	end

end

function ENT:Use(activator, caller)

	if !self:GetParent() then return end

	if self:GetPressedPaperAmount() == 100 then

		if GAMEMODE.Inv:GivePlayerItem( caller, "Counterfeit Money", 1 ) then
			caller:AddNote( "You collected some counterfeit money." )
			self:SetPressedPaperAmount( 0 )
		else
			caller:AddNote( "Your inventory is full!" )
		end

	else

		self:GetParent().NextPress = CurTime() + 3
		self:GetParent():SetPressAttached( false )
		self:SetParent( nil )
		self:SetPos( self:GetPos() + Vector( 0,0,20 ) )

	end

end
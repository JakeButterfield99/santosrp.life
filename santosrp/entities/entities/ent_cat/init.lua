AddCSLuaFile("cl_init.lua")
AddCSLuaFile("shared.lua")

include("shared.lua")

function ENT.Initialize(self)

	self:SetModel("models/feline.mdl")
	self:SetUseType(SIMPLE_USE)
	self:PhysicsInit(SOLID_VPHYSICS)
	self:SetMoveType(MOVETYPE_VPHYSICS)
	self:SetSolid(SOLID_VPHYSICS)

	local phys = self:GetPhysicsObject()

	if phys:IsValid() then

		phys:Wake()
 
	end

end

function ENT:Use( activator, caller )

	if GAMEMODE.Jobs:GetPlayerJobID( caller ) == JOB_FIREFIGHTER then

		caller:AddMoney( 200 )
		caller:AddNote( "You earned a $200 bonus for saving the cat!" )
		caller:AddNote( "This bonus has been sent to your bank account." )		

		self:Remove()

	end

end
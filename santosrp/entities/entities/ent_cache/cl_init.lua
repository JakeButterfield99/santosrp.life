include('shared.lua')

net.Receive("PBankRB_CacheTaken", function() 

    local Ent, Bool = net.ReadEntity(), net.ReadBool()
	
	Ent.CacheTaken = Bool

end)

function ENT:Think()
	if IsValid(self.Cache) then
		self.Cache:SetAngles(self.Entity:GetAngles())
		self.Cache:SetPos( self.Entity:GetPos() + self.Entity:GetUp()*-10)
		self.Cache:SetParent(self.Entity)
	end
	if !IsValid(self.Cache) and !self.CacheTaken then
		self.Cache = ClientsideModel(PDRobberyConfig.WeaponCacheModel, RENDERGROUP_OPAQUE);
		self.Cache:PhysicsInit( SOLID_NONE )
		self.Cache:SetMoveType( MOVETYPE_NONE )
		self.Cache:SetSolid( SOLID_NONE )
	elseif IsValid(self.Cache) and self.CacheTaken then
	    self.Cache:Remove()
	end
end

function ENT:Initialize ( )		
	self.Cache = ClientsideModel(PDRobberyConfig.WeaponCacheModel, RENDERGROUP_OPAQUE);
	self.Cache:PhysicsInit( SOLID_NONE )
	self.Cache:SetMoveType( MOVETYPE_NONE )
	self.Cache:SetSolid( SOLID_NONE )
end

function ENT:Draw()	
	self:DrawModel();
end


function ENT:OnRemove( )
    if (IsValid(self.Cache)) then
	    self.Cache:Remove()
    end
end	

include("shared.lua")

function ENT:Draw()

	self:DrawModel()

end

local white = Color(255,255,255)
local darkblue = Color(3,20,36)
local blue = Color(48,65,93)
local lightblue = Color(142,174,189)
local red = Color(207,103,102)

function ENT:UpdatePaper( strText )

	net.Start("oakrp_updatepaper")
		net.WriteString( strText )
		net.WriteEntity( self )
	net.SendToServer()

end

function ENT:DrawPaper()

	local previousText = self:GetPaperText()

	local frame = vgui.Create("DFrame")
	frame:SetSize(400,600)
	frame:SetTitle("Paper")
	frame:Center()
	frame:ShowCloseButton(true)
	frame:SetVisible(true)
	frame:MakePopup()
	frame.Paint = function(s, w, h)
		draw.RoundedBox(0,0,0,w,h,darkblue)
		draw.RoundedBox(0,5,5,w-10,h-10,blue)
	end

	local text = vgui.Create( "DTextEntry", frame )
	text:SetPos( 10, 30 )
	text:SetSize( frame:GetWide() - 20, frame:GetTall() - 100 )
	text:SetMultiline( true )
	text:SetText( previousText or "" )

	local button = vgui.Create( "DButton", frame )
	button:SetText( "Update Paper" )
	button:SetPos( (frame:GetWide() / 2) - 50 , frame:GetTall() - 65 )
	button:SetSize( 100, 50 )
	button.DoClick = function()
		self:UpdatePaper( text:GetValue() or "" )
	end

end

net.Receive("oakrp_openpaper",function()

	local ent = net.ReadEntity()

	ent:DrawPaper()

end)
AddCSLuaFile( "cl_init.lua" )
AddCSLuaFile( "shared.lua" )
include('shared.lua')

function ENT:Initialize()
	self.Entity:SetModel("models/hunter/blocks/cube025x025x025.mdl")	
	self.Entity:PhysicsInit( SOLID_VPHYSICS )
	self.Entity:SetMoveType( MOVETYPE_VPHYSICS )
	self.Entity:SetSolid( SOLID_VPHYSICS )
	self.Entity:SetUseType( SIMPLE_USE )
	self:GetPhysicsObject():EnableMotion(false)
end

function ENT:Use( activator, caller )
	if self.Entity:GetTable().Tapped and self.Entity:GetTable().Tapped > CurTime() then return false; end	
	self.Entity:GetTable().Tapped = CurTime() + 5;
	if !activator:IsPlayer() then return false; end
	
	if activator:IsGovernment() or (activator:IsBanker() and PDRobberyConfig.BankersSecurityDoor) or activator.HasKeycard then
		if IsValid(self.Door) then
			self:EmitSound("buttons/button3.wav")
			if self.Door.Opened then
				self.Door.Opened = false
				self.Door:Fire("unlock", "", 0)
				self.Door:Fire("close", "", 0)
				self.Door:Fire("lock", "", 0)
			else
				self.Door.Opened = true
				self.Door:Fire("unlock", "", 0)
				self.Door:Fire("open", "", 0)
				self.Door:Fire("lock", "", 0)
			end
		end
	else
		self:EmitSound("buttons/button2.wav")
	end
end

function ENT:Think()
end

function ENT:Touch(TouchEnt)
end

function ENT:OnRemove()
 	if IsValid(self.Door) then	
		if IsValid(self.Door.Scanner1) then
			self.Door.Scanner1:Remove()
		end
		if IsValid(self.Door.Scanner2) then
			self.Door.Scanner2:Remove()
		end	
		self.Door:Remove()
	end
end
include('shared.lua')
function ENT:Think ()
	if ( !self.ScannerEffect || !self.ScannerEffect:IsValid() ) then		
		self.ScannerEffect = ClientsideModel("models/props_lab/eyescanner.mdl", RENDERGROUP_OPAQUE);
		self.ScannerEffect:PhysicsInit( SOLID_NONE )
		self.ScannerEffect:SetMoveType( MOVETYPE_NONE )
		self.ScannerEffect:SetSolid( SOLID_NONE )
	end
	
	if (IsValid(self.ScannerEffect)) then
	    local ang = self.Entity:GetAngles()
		self.ScannerEffect:SetAngles(ang+Angle(0,180,0))
		self.ScannerEffect:SetPos( self.Entity:GetPos())
		self.ScannerEffect:SetParent(self.Entity)	
	end	
end

function ENT:Draw()
	--self:DrawModel()	
end

function ENT:OnRemove( )
	if (IsValid(self.ScannerEffect)) then
		self.ScannerEffect:Remove();
	end
end	

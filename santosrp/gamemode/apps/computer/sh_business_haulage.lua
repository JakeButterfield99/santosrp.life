--[[
	Name: sh_mayor_taxes.lua
	For: TalosLife
	By: TalosLife
]]--

local App = {}
App.Name = "Haulage Inc."
App.ID = "haulage.exe"
App.Panel = "SRPComputer_AppWindow_Haulage"
App.Icon = "santosrp/computer/icon_ss.png"
App.DekstopIcon = true
App.StartMenuIcon = true
App.Business = "Haulage Inc."

--App code
--End app code

GM.Apps:Register( App )

if SERVER then return end

local Panel = {}
function Panel:Init()
	self:GetParent():SetTitle( App.Name )
	self:GetParent():SetSize( 410, 380 )
	self:GetParent():SetPos( 75, 75 )

	self.BusinessOwned = GAMEMODE.Business:PlayerOwnsBusiness( App.Business )

	if !self.BusinessOwned then
		self.m_pnlErrorLabel = vgui3D.Create("DLabel", self)
	else

		self.m_pnlTextLabel = vgui3D.Create("DLabel", self)
		self.m_pnlTextLabel:SetFont("DermaLarge")

		self.m_pnlSupplyLabel = vgui3D.Create("DLabel", self)
		self.m_pnlSupplyLabel:SetFont("Trebuchet18")

		self.m_pnlSupplyAmount = vgui3D.Create("DLabel", self)

		self.m_pnlValueLabel = vgui3D.Create("DLabel", self)
		self.m_pnlValueLabel:SetFont("Trebuchet18")

		self.m_pnlValueAmount = vgui3D.Create("DLabel", self)

		self.m_pnlBuyLabel = vgui3D.Create("DLabel", self)
		self.m_pnlBuyLabel:SetFont("Trebuchet18")

		self.m_pnlBuyAmount = vgui3D.Create("DLabel", self)

		self.m_pnlSellLabel = vgui3D.Create("DLabel", self)
		self.m_pnlSellLabel:SetFont("Trebuchet18")

		self.m_pnlSellAmount = vgui3D.Create("DLabel", self)

		self.btnBuyButton = vgui3D.Create("DButton", self)
		self.btnBuyButton.DoClick = function()
			GAMEMODE.Business:RequestJob( App.Business, "buy" )
		end

		self.btnSellButton = vgui3D.Create("DButton", self)
		self.btnSellButton.DoClick = function()

		end

	end

	self:GetData()

end

function Panel:GetData()

	if !self.BusinessOwned then
		self.m_pnlErrorLabel:SetText("You do not own this business!")
	else

		local bData = GAMEMODE.Business:GetPlayerBusiness( App.Business )

		self.m_pnlTextLabel:SetText( App.Name )

		self.m_pnlSupplyLabel:SetText("Supplies")
		self.m_pnlValueLabel:SetText("Value")

		self.m_pnlBuyLabel:SetText("Buy Price:")
		self.m_pnlSellLabel:SetText("Sell Bonus:")

		if bData["Supplies"] == 100 then
			self.m_pnlSellAmount:SetText("x3")
		elseif bData["Supplies"] >= 50 then
			self.m_pnlSellAmount:SetText("x2")
		else
			self.m_pnlSellAmount:SetText("x1")
		end

		self.m_pnlSupplyAmount:SetText( bData["Supplies"] )
		self.m_pnlValueAmount:SetText( "$" .. bData["Value"] )

		self.m_pnlBuyAmount:SetText( "$" .. GAMEMODE.Business:GetBuyPrice( App.Business ) )

		self.btnBuyButton:SetText("Buy Supplies")
		self.btnSellButton:SetText("Sell Supplies")

	end

	self:GetParent():InvalidateLayout()

end

function Panel:PerformLayout( intW, intH )
	local padding = 10

	if !self.BusinessOwned then
		self.m_pnlErrorLabel:SizeToContents()
		self.m_pnlErrorLabel:SetPos( (intW/2 - (self.m_pnlErrorLabel:GetWide()/2)), intH/2 )
		return
	end

	// Layer 1

	self.m_pnlTextLabel:SizeToContents()
	self.m_pnlTextLabel:SetPos( (intW/2 - (self.m_pnlTextLabel:GetWide()/2)), 10 )

	// Layer 2

	self.m_pnlSupplyLabel:SizeToContents()
	self.m_pnlSupplyLabel:SetPos( 20, 70 )

	self.m_pnlSupplyAmount:SizeToContents()
	self.m_pnlSupplyAmount:SetPos( 20, 90 )

	self.m_pnlValueLabel:SizeToContents()
	self.m_pnlValueLabel:SetPos( intW - self.m_pnlValueLabel:GetWide() - 20 , 70 )

	self.m_pnlValueAmount:SizeToContents()
	self.m_pnlValueAmount:SetPos( intW - self.m_pnlValueAmount:GetWide() - 20 , 90 )

	// Layer 3

	self.m_pnlBuyLabel:SizeToContents()
	self.m_pnlBuyLabel:SetPos( 20, intH - 120 )

	self.m_pnlBuyAmount:SizeToContents()
	self.m_pnlBuyAmount:SetPos( 20, intH - 100 )

	self.m_pnlSellLabel:SizeToContents()
	self.m_pnlSellLabel:SetPos( intW - self.m_pnlSellLabel:GetWide() - 20, intH - 120 )

	self.m_pnlSellAmount:SizeToContents()
	self.m_pnlSellAmount:SetPos( intW - self.m_pnlSellAmount:GetWide() - 20, intH - 100 )
	
	// Layer 4

	self.btnBuyButton:SetPos( 20, intH - 80 )
	self.btnBuyButton:SetSize( 100, 60 )

	self.btnSellButton:SetPos( intW - 100 - 20, intH - 80 )
	self.btnSellButton:SetSize( 100, 60 )

end
vgui.Register( "SRPComputer_AppWindow_Haulage", Panel, "EditablePanel" )
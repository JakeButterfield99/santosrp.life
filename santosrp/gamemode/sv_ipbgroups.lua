require( "mysqloo" )
GM.IPBGroups = (GM or GAMEMODE).IPBGroups or {}

function GM.IPBGroups:Connect( ip, username, password, table )
	if self.db then return end
	self.db = mysqloo.connect( ip, username, password, table )
	self.db.onConnected = function( db )
		print("==============================================")
		print( "[SantosRP.Life] - Connected to IPB database." )
		print("==============================================")
	end

	self.db.onConnectionFailed = function( err )
		print("===============================================")
		print( "[SantosRP.Life] - Failed to connect to database. Error: " .. err )
		print("===============================================")
	end
	
	self.db:connect()
end

function GM.IPBGroups:Query( query, ply, callback )
	local q = self.db:query( query )
	q.onSuccess = function( q, data )
		if callback then
			callback( data, ply )
		end
	end
	
	q.onError = function( query, err )
		print("==============================")
		print( "[GRP] - MySQL Error: ".. err )
		print("==============================")
		
		-- Check for connection error.
		if self.db:status() ~= mysqloo.DATABASE_CONNECTED then
			print("=============================================")
			print( "[GRP] - Attempting to reconnect to database." )
			print("=============================================")
			-- Attempt to reconnect to the database.
			self.db:connect()
			self.db:wait()

			-- Check for connection.
			if self.db:status() == mysqloo.DATABASE_CONNECTED then
				print("==============================================")
				print( "[GRP] - Connected to database resuming query." )
				print("==============================================")
				-- Retry the query.
				query:start()
			else
				print("====================================")
				print( "[GRP] - Database connection failed." )
				print("====================================")
				return
			end
		end
	end
	
	q:start()
end

-- Allowed groups in whitelist.
local WhitelistGroups = { 
	19,
}

-- Array that contains all of the players and their associated groups.
local playerGroups = playerGroups or {}
local forumGroups = forumGroups or {}

local function playerGroupsLoaded( ply )
	-- Check if the player's groups are saved.
	if playerGroups[ply:SteamID64()] then
		return true
	end
	return false
end

local function removePlayerGroups( ply )
	-- Verify player groups have been loaded.
	if playerGroupsLoaded( ply ) then
		-- Remove all of their data.
		playerGroups[ply:SteamID64()] = nil
	end
end

function InGroup( ply, groupID )
	-- Verify the groups have been loaded
	-- for this player.
	if playerGroupsLoaded( ply ) then
		-- Check if the user has the specified group.
		if table.HasValue( playerGroups[ply:SteamID64()], groupID ) then
			return true
		end
	end

	return false
end

local function IPBGroupsSpawn( ply )
	if ply:IsBot() then return end
	-- Query to check for an existing IPB account and retrieve their groups.
	local query = "SELECT member_id, member_group_id, mgroup_others FROM core_members WHERE steamid='".. ply:SteamID64().. "';"
	
	-- Perform the query 
	GAMEMODE.IPBGroups:Query(query, ply, function( data, ply )
		-- Verify that data was returned.
		if data[1] then
			local row = data[1]

			local forumID = tonumber( row["member_id"] )

			-- Set up the player's table of groups.
			playerGroups[ply:SteamID64()] = { }
			-- Add the player's main group.
			table.insert(playerGroups[ply:SteamID64()],row["member_group_id"])
		
			-- Check if they have other groups. 
			if row["mgroup_others"] ~= "" then
				-- Split the groups and insert them into the table.
				for g in string.gmatch(row["mgroup_others"], '([^,]+)') do
					-- Add the group.
					table.insert(playerGroups[ply:SteamID64()],tonumber(g))
				end
			end
			
			-- Check if any of the groups are in the whitelist.
			for k, g in pairs( playerGroups[ply:SteamID64()] ) do
				-- Check if group is whitelisted.
				if table.HasValue( WhitelistGroups, g ) then
					-- Allow the player to join.
					forumGroups[ply:SteamID64()] = forumID
					return true
				end
			end

			for something, data in ipairs(GAMEMODE.Config.ServerGroups) do

				local cont
				for _, id in pairs(data.ids) do
					if InGroup(ply, id) then
						serverguard.player:SetRank(ply, data.group)
						cont = true
						break
					end
				end
				
				if cont then break end
			end

		end
		
		-- if ply:SteamID64() == "76561198045169987" then return true end
		-- The player will be kicked if they are not allowed in the server. 
		--print( ply:Nick().. " kicked from server. (Not whitelisted)" )
		--ply:Kick( "You are not whitelisted" )

	end)
end

hook.Add( "GamemodePlayerSelectCharacter", "ApplyForumID", function( pPlayer )

	local group = forumGroups[pPlayer:SteamID64()] or 0
	GAMEMODE.Player:SetSharedGameVar( pPlayer, "forum_id", group, true )

end )

local function IPBGroupsDisconnect( ply )
	-- Remove the player's groups from the table.
	removePlayerGroups( ply )
end

GM.IPBGroups:Connect( "xxx", "xxx", "xxx", "xxx" )
-- Add hook for IPB group system. ( Player join )
hook.Add( "PlayerInitialSpawn", "IPBGroupsSpawn", IPBGroupsSpawn )
-- Add hook for IPB group system. ( Player disconnect )
hook.Add( "PlayerDisconnected", "IPBGroupsDisconnect", IPBGroupsDisconnect )

hook.Add( "GamemodeIsPlayerJobWhitelisted", "IPBGroups", function( pPlayer, intJobID )
	local enum = GAMEMODE.Jobs:GetJobByID( intJobID ).Enum
	if not GAMEMODE.Config.IPBJobWhitelist[enum] then return end
	for k, v in pairs( GAMEMODE.Config.IPBJobWhitelist[enum] ) do
		if InGroup( pPlayer, v ) then
			return true
		end
	end
		
	return false
end )

local pmeta = FindMetaTable("Player")

function pmeta:GetForumIDRaw()

	return forumGroups[self:SteamID64()]

end
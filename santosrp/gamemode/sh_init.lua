--[[
	Name: sh_init.lua
	For: SantosRP
	By: SantosRP
]]--

GM.Name 	= "SantosRP"
GM.Author 	= "Jake Butts"
GM.Email 	= ""
GM.Website 	= ""

function GM:PrintDebug( intLevel, ... )
	--print( ... )
end

include( SERVER and "sv_manifest.lua" or "cl_manifest.lua" )

if not debug.getregistry().Player.CheckGroup then
	debug.getregistry().Player.CheckGroup = function()
		return true
	end
end

if game.SinglePlayer() then
	debug.getregistry().Player.SteamID64 = function()
		return "1234567890"
	end
end

hook.Remove( "PlayerTick", "TickWidgets" )

--Precache models
for k, v in pairs( GM.Config.PlayerModels.Male ) do
	util.PrecacheModel( k )
end

for k, v in pairs( GM.Config.PlayerModels.Female ) do
	util.PrecacheModel( k )
end

for k, v in pairs( GM.Config.PlayerModelOverloads.Male or {} ) do
	util.PrecacheModel( k )
end

for k, v in pairs( GM.Config.PlayerModelOverloads.Female or {} ) do
	util.PrecacheModel( k )
end
--[[
	Name: weapons_fire.lua
	For: OakRoleplay
	By: Jake Butts
]]--

local Item = {}
Item.Name = "Fire Axe"
Item.Desc = "A fire axe, used to break items or force open doors."
Item.Type = "type_weapon"
Item.Model = "models/props_forest/axe.mdl"
Item.JobItem = "JOB_FIREFIGHTER" --This item can only be possessed by by players with this job
Item.Weight = 15
Item.Volume = 11
Item.CanDrop = false
Item.CanEquip = true
Item.EquipSlot = "PrimaryWeapon"
Item.EquipGiveClass = "weapon_fireaxe"

GM.Inv:RegisterItem( Item )

local Item = {}
Item.Name = "Government Issued Metal Ladder"
Item.Desc = "Climb Even Bigger Things."
Item.Type = "type_furniture"
Item.Model = "models/props_c17/metalladder004.mdl"
Item.JobItem = "JOB_FIREFIGHTER"
Item.Weight = 100
Item.Volume = 50
Item.HealthOverride = 3000
Item.CanDrop = true
Item.DropClass = "prop_physics"

GM.Inv:RegisterItem( Item )

local Item = {}
Item.Name = "Government Issued Large Metal Ladder"
Item.Desc = "Climb Even Bigger Things."
Item.Type = "type_furniture"
Item.Model = "models/props_c17/metalladder003.mdl"
Item.JobItem = "JOB_FIREFIGHTER"
Item.Weight = 100
Item.Volume = 50
Item.HealthOverride = 3000
Item.CanDrop = true
Item.DropClass = "prop_physics"

GM.Inv:RegisterItem( Item )
--[[
	Name: misc_weapons.lua
	For: TalosLife
	By: TalosLife
]]--

local Item = {}
Item.Name = "Civ Radio"
Item.Desc = "Civ Radio"
Item.Type = "type_weapon"
Item.Model = "models/gspeak/funktronics.mdl"
Item.Weight = 1
Item.Volume = 1
Item.CanDrop = true
Item.CanEquip = true
Item.Illegal = true
Item.EquipSlot = "AltWeapon"
Item.EquipGiveClass = "weapon_gspeak_radio_civ"
Item.DropClass = "weapon_gspeak_radio_civ"
GM.Inv:RegisterItem( Item )

local Item = {}
Item.Name = "Pepper Spray"
Item.Desc = "Pepper Spray. Somebodies eyes will burn with this."
Item.Type = "type_weapon"
Item.Model = "models/weapons/Custom/pepperspray.mdl"
Item.Weight = 10
Item.Volume = 10
Item.CanDrop = true
Item.CanEquip = true
Item.EquipSlot = "AltWeapon"
Item.EquipGiveClass = "weapon_pepperspray"
Item.DropClass = "weapon_pepperspray"
GM.Inv:RegisterItem( Item )

local Item = {}
Item.Name = "Government Issue Pepper Spray"
Item.Desc = "Pepper Spray. Issued for Government use only."
Item.Type = "type_weapon"
Item.Model = "models/weapons/Custom/pepperspray.mdl"
Item.Weight = 10
Item.Volume = 10
Item.CanDrop = true
Item.JobItem = "JOB_SSERVICE" --This item can only be possessed by by players with this job
Item.CanEquip = true
Item.EquipSlot = "AltWeapon"
Item.EquipGiveClass = "weapon_pepperspray"
Item.DropClass = "weapon_pepperspray"
GM.Inv:RegisterItem( Item )

local Item = {}
Item.Name = "Police Issue Pepper Spray"
Item.Desc = "Pepper Spray. Issued for Police use only."
Item.Type = "type_weapon"
Item.Model = "models/weapons/Custom/pepperspray.mdl"
Item.Weight = 10
Item.Volume = 10
Item.CanDrop = true
Item.JobItem = "JOB_POLICE" --This item can only be possessed by by players with this job
Item.CanEquip = true
Item.EquipSlot = "AltWeapon"
Item.EquipGiveClass = "weapon_pepperspray"
Item.DropClass = "weapon_pepperspray"
GM.Inv:RegisterItem( Item )

local Item = {}
Item.Name = "Molotov Cocktail"
Item.Desc = "A molotov cocktail."
Item.Type = "type_weapon"
Item.Model = "models/props_junk/garbage_glassbottle002a.mdl"
Item.Weight = 3
Item.Volume = 3
Item.CanEquip = true
Item.EquipSlot = "AltWeapon"
Item.EquipGiveClass = "weapon_molotov"
GM.Inv:RegisterItem( Item )

--[[

local Item = {}
Item.Name = "Flash Grenade"
Item.Desc = "A flash bang."
Item.Type = "type_weapon"
Item.Model = "models/weapons/w_eq_flashbang.mdl"
Item.Weight = 3
Item.Volume = 3
Item.CanDrop = true
Item.CanEquip = true
Item.Illegal = true
Item.EquipSlot = "AltWeapon"
Item.EquipGiveClass = "weapon_fas_grenadeflash"
Item.DropClass = "weapon_fas_grenadeflash"

Item.CraftingEntClass = "ent_assembly_table"
Item.CraftingTab = "Weapons"
Item.CraftSkill = "Gun Smithing"
Item.CraftSkillLevel = 23
Item.CraftSkillXP = 2
Item.CraftRecipe = {
	["Metal Bracket"] = 4,
	["Metal Plate"] = 8,
	["Smokeless Gunpowder"] = 4,
}
GM.Inv:RegisterItem( Item )

]]

local Item = {}
Item.Name = "Lockpick"
Item.Desc = "A lockpick. Can unlock doors and free handcuffed players."
Item.Type = "type_weapon"
Item.Model = "models/weapons/w_crowbar.mdl"
Item.Weight = 1
Item.Volume = 1
Item.CanDrop = true
Item.CanEquip = true
Item.Illegal = true
Item.EquipSlot = "AltWeapon"
Item.EquipGiveClass = "weapon_lockpick"
Item.DropClass = "weapon_lockpick"

Item.CraftingEntClass = "ent_assembly_table"
Item.CraftingTab = "Weapons"
Item.CraftSkill = "Gun Smithing"
Item.CraftSkillLevel = 1
Item.CraftSkillXP = 2
Item.CraftRecipe = {
	["Metal Bracket"] = 1,
	["Metal Plate"] = 2,
	["Crowbar"] = 1,
}
GM.Inv:RegisterItem( Item )


local Item = {}
Item.Name = "Zip Tie"
Item.Desc = "A zip tie. Can restrain other players."
Item.Type = "type_weapon"
Item.Model = "models/katharsmodels/handcuffs/handcuffs-1.mdl"
Item.Weight = 1
Item.Volume = 1
Item.CanDrop = true
Item.CanEquip = true
Item.Illegal = true
Item.EquipSlot = "AltWeapon"
Item.EquipGiveClass = "weapon_ziptie"
Item.DropClass = "weapon_ziptie"

Item.CraftingEntClass = "ent_assembly_table"
Item.CraftingTab = "Weapons"
Item.CraftSkill = "Gun Smithing"
Item.CraftSkillLevel = 2
Item.CraftSkillXP = 2
Item.CraftRecipe = {
	["Chunk of Plastic"] = 8,
	["Metal Bracket"] = 2,
	["Pliers"] = 3,
}
GM.Inv:RegisterItem( Item )
--[[
	Haulage Inc.
]]--

local Item = {}
Item.Name = "Business Supplies"
Item.Desc = "A collection of business supplies"
Item.Model = "models/props_junk/wood_crate002a.mdl"
Item.Weight = 200
Item.Volume = 150
Item.HealthOverride = 2500
Item.CanDrop = true
Item.CollidesWithCars = true
Item.DropClass = "prop_physics"

GM.Inv:RegisterItem( Item )

local Item = {}
Item.Name = "Electric Supplies"
Item.Desc = "Electric Supplies, deliver to Electronics Store"
Item.Model = "models/props_junk/wood_crate002a.mdl"
Item.Weight = 200
Item.Volume = 150
Item.HealthOverride = 2500
Item.CanDrop = true
Item.CollidesWithCars = true
Item.DropClass = "prop_physics"

GM.Inv:RegisterItem( Item )

local Item = {}
Item.Name = "Hardware Supplies"
Item.Desc = "Hardware Supplies, deliver to Hardware Store"
Item.Model = "models/props_junk/wood_crate002a.mdl"
Item.Weight = 200
Item.Volume = 150
Item.HealthOverride = 2500
Item.CanDrop = true
Item.CollidesWithCars = true
Item.DropClass = "prop_physics"

GM.Inv:RegisterItem( Item )

local Item = {}
Item.Name = "Supermarket Supplies"
Item.Desc = "Supermarket Supplies, deliver to Supermarket"
Item.Model = "models/props_junk/wood_crate002a.mdl"
Item.Weight = 200
Item.Volume = 150
Item.HealthOverride = 2500
Item.CanDrop = true
Item.CollidesWithCars = true
Item.DropClass = "prop_physics"

GM.Inv:RegisterItem( Item )

local Item = {}
Item.Name = "Clothing Supplies"
Item.Desc = "Clothing Supplies, deliver to Clothing Store"
Item.Model = "models/props_junk/wood_crate002a.mdl"
Item.Weight = 200
Item.Volume = 150
Item.HealthOverride = 2500
Item.CanDrop = true
Item.CollidesWithCars = true
Item.DropClass = "prop_physics"

GM.Inv:RegisterItem( Item )
--[[
	Name: drugs_other_effects.lua
	For: TalosLife
	By: TalosLife
]]--


--[[ Broken Bone Effect ]]--
local brokenEffect = {}
brokenEffect.Name = "broken"
brokenEffect.NiceName = "Broken Bone"
brokenEffect.EffectName = "Broken Bone"
brokenEffect.Image = "santosrp/ae_icons/broken leg(s) 48x48.png"
brokenEffect.MaxPower = 50

function brokenEffect:OnStart( pPlayer )
	if SERVER then
		//pPlayer:EmitSound( "ambient/voices/cough".. math.random(1, 4)..".wav", 75, 95 )
		//pPlayer:ViewPunch( Angle(math.random(6, 12), 0, 0) )
	end
end

function brokenEffect:OnStop( pPlayer )
	if SERVER then
		if GAMEMODE.PlayerDamage:PlayerHasBrokenBone( pPlayer ) then
			GAMEMODE.Drugs:PlayerApplyEffect( pPlayer, "broken", 3 *60, 2 )
		end
	end
end

if CLIENT then
	function brokenEffect:RenderScreenspaceEffects()
	end

	function brokenEffect:GetMotionBlurValues( intW, intH, intForward, intRot )
		return intW, intH, intForward, intRot
	end
end

GM.Drugs:RegisterEffect( brokenEffect )
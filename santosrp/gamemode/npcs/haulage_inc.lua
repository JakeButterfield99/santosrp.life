--[[
	Name: haulage_inc.lua
	For: SantosRP.Life
	By: Jake Butts
]]--

local NPCMeta = {}
NPCMeta.Name = "Haulage Inc."
NPCMeta.UID = "haulage_inc"
NPCMeta.SubText = "Haulage Inc Company"
NPCMeta.Model = "models/player/eli.mdl"
NPCMeta.Sounds = {
	StartDialog = {
		"vo/eli_lab/mo_airlock03.wav",
	},
	EndDialog = {
		"vo/eli_lab/mo_anyway04.wav",
		"vo/eli_lab/mo_gowithalyx02.wav",
	}
}
--[itemID] = priceToBuy,
NPCMeta.ItemsForSale = {}

--[itemID] = priceToSell,
NPCMeta.ItemsCanBuy = {}

function NPCMeta:OnPlayerTalk( entNPC, pPlayer )
	GAMEMODE.Net:ShowNPCDialog( pPlayer, "haulage_inc" )
	
	if (entNPC.m_intLastSoundTime or 0) < CurTime() then
		local snd, _ = table.Random( self.Sounds.StartDialog )
		entNPC:EmitSound( snd, 60 )
		entNPC.m_intLastSoundTime = CurTime() +2
	end
end

function NPCMeta:OnPlayerEndDialog( pPlayer )
	if not pPlayer:WithinTalkingRange() then return end
	if pPlayer:GetTalkingNPC().UID ~= self.UID then return end

	if (pPlayer.m_entTalkingNPC.m_intLastSoundTime or 0) < CurTime() then
		local snd, _ = table.Random( self.Sounds.EndDialog )
		pPlayer.m_entTalkingNPC:EmitSound( snd, 60 )
		pPlayer.m_entTalkingNPC.m_intLastSoundTime = CurTime() +2
	end

	pPlayer.m_entTalkingNPC = nil
end

if SERVER then
	--RegisterDialogEvents is called when the npc is registered! This is before the gamemode loads so GAMEMODE is not valid yet.
	function NPCMeta:RegisterDialogEvents()
	end
elseif CLIENT then
	function NPCMeta:RegisterDialogEvents()
		GM.Dialog:RegisterDialog( "haulage_inc", self.StartDialog, self )
	end
	
	function NPCMeta:StartDialog()
		GAMEMODE.Dialog:ShowDialog()
		GAMEMODE.Dialog:SetModel( self.Model )
		GAMEMODE.Dialog:SetTitle( self.Name )
		GAMEMODE.Dialog:SetPrompt( "Welcome to Haulage Inc., What can I do for ya?" )

		GAMEMODE.Dialog:AddOption( "I have some supplies to store", function()
			GAMEMODE.Net:BusinessStoreSupplies( self.Name )
			GAMEMODE.Dialog:HideDialog()
		end )

		GAMEMODE.Dialog:AddOption( "I want to sell some supplies", function()
			GAMEMODE.Net:RequestJob( self.Name, "sell" )
			GAMEMODE.Dialog:HideDialog()
		end )

		GAMEMODE.Dialog:AddOption( "Never mind, I have to go.", function()
			GAMEMODE.Net:SendNPCDialogEvent( self.UID.. "_end_dialog" )
			GAMEMODE.Dialog:HideDialog()
		end )
	end
end

GM.NPC:Register( NPCMeta )
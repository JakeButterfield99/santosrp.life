--[[
	Name: cl_menu_large_sign.lua
	For: TalosLife
	By: TalosLife
]]--

local Panel = {}
function Panel:Init()
	self:SetTitle( "Sign Menu" )

	self.m_pnlApplyBtn = vgui.Create( "SRP_Button", self )
	self.m_pnlApplyBtn:SetFont( "CarMenuFont" )
	self.m_pnlApplyBtn:SetText( "Apply" )
	self.m_pnlApplyBtn:SetAlpha( 150 )
	self.m_pnlApplyBtn.DoClick = function()
		self.m_entSign:RequestSetText( self.m_pnlText:GetValue() )
	end

	self.m_pnlTakeBtn = vgui.Create( "SRP_Button", self )
	self.m_pnlTakeBtn:SetFont( "CarMenuFont" )
	self.m_pnlTakeBtn:SetText( "Take Sign" )
	self.m_pnlTakeBtn:SetAlpha( 150 )
	self.m_pnlTakeBtn.DoClick = function()
		self.m_entSign:RequestTakeSign()
	end

	self.m_pnlText = vgui.Create( "DTextEntry", self )
	self.m_pnlText:SetMultiline( true )
	self.m_pnlText:SetFont( "DermaLarge" )

	function self.m_pnlText:AllowInput( intKey )
		local text = self:GetValue():sub(0, self:GetCaretPos())
		local lines = string.Explode('\n', self:GetValue())

		local line = 1
		for v in string.gmatch(text, '\n') do
			line = line + 1
		end

		if string.len(lines[line]) >= 20 then
			return true
		end
	end

	function self.m_pnlText:OnKeyCodeTyped( intKey )
		local lines = string.Explode('\n', self:GetValue())
		if #lines +1 > 13 and intKey == KEY_ENTER then
			return true
		end
	end
end

function Panel:SetEntity( eEnt )
	if not eEnt.GetText then return end
	self.m_entSign = eEnt
	self.m_pnlText:SetValue( eEnt:GetText() )
end

function Panel:Think()
	if not IsValid( self.m_entSign ) then
		self:Remove()
	end
end

function Panel:PerformLayout( intW, intH )
	DFrame.PerformLayout( self, intW, intH )

	self.m_pnlApplyBtn:SetSize( intW /2, 32 )
	self.m_pnlApplyBtn:SetPos( 0, intH -self.m_pnlApplyBtn:GetTall() )

	self.m_pnlTakeBtn:SetSize( intW /2, 32 )
	self.m_pnlTakeBtn:SetPos( intW /2, intH -self.m_pnlTakeBtn:GetTall() )

	self.m_pnlText:SetPos( 0, 24 )
	self.m_pnlText:SetSize( intW, intH -24 -self.m_pnlTakeBtn:GetTall() )
end
vgui.Register( "SRPLargeSignMenu", Panel, "SRP_Frame" )
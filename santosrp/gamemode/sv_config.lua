--[[
	Name: sv_config.lua
	For: SantosRP
	By: SantosRP 
]]--

--[[ SQL Settings ]]--
if DEV_SERVER then
	GM.Config.SQLHostName = "xxx"
	GM.Config.SQLUserName = "xxx"
	GM.Config.SQLPassword = "xxx"
	GM.Config.SQLDBName = "xxx"
else
	GM.Config.SQLHostName = "xxx"
	GM.Config.SQLUserName = "xxx"
	GM.Config.SQLPassword = "xxx"
	GM.Config.SQLDBName = "xxx"
end

GM.Config.SQLSnapshotRate = 5 *60 --Lower = less time between updates to sql. This value should be set with respect to the number of workers in use!
GM.Config.SQLReconnectInterval = 1 *60 --Time to wait before retrying a lost sql connection
GM.Config.SQLNumWriteWorkers = 1 --Number of connections to open to the sql server for writing player data with

--[[ IPB Settings ]]--
GM.Config.ServerRegion = "US" --Options: US, EU
GM.Config.IPBJobWhitelist = {
	["JOB_POLICE"] = { 11 },
	["JOB_FIREFIGHTER"] = {  },
	["JOB_EMS"] = { 12 },
	["JOB_MAYOR"] = { 13 },
	["JOB_PROSECUTOR"] = {  },
	["JOB_LAWYER"] = {  },
	["JOB_JUDGE"] = {  },
	["JOB_FINANCE"] = {  },
}

GM.Config.ServerGroups = {
	{ group = "founder", 		ids = {8} },
	{ group = "pl", 			ids = {9} },
	{ group = "owner", 			ids = {7} },
	{ group = "sm", 			ids = {10} },
	{ group = "admin", 			ids = { } },
	{ group = "mod", 			ids = { } },
}

--[[ ServerNet Settings ]]--
GM.Config.ServerNetUseTLS_1_2 = false
GM.Config.ServerNetExtraAuth = false
GM.Config.ServerNetExtraAuthKey = ""
GM.Config.ServerNetPort = 37015
GM.Config.ServerNetPool = {
	DEV_SERVER and "107.150.52.226" or nil,
	not DEV_SERVER and (GM.Config.ServerRegion == "US" and "107.150.52.226" or "107.150.52.226") or nil,
}

--[[ Global Loadout Settings ]]--
GM.Config.GlobalLoadout = {
	"weapon_physgun",
	--"weapon_physcannon",
	--"weapon_fists",
	"weapon_keys",
	"weapon_srphands",
	"weapon_idcard",
}

--[[ Car Settings ]]--
GM.Config.UseCustomVehicleDamage = true
GM.Config.BaseCarFuelConsumption = 100

--[[ Robbery Settings ]]
GM.Config.RobberyMoneyAmount = 5000
GM.Config.RobberyMaxTime = 5 *60
GM.Config.RobberyMinTime = 3 *60
GM.Config.RobberyDelay = 10 *60
GM.Config.RobberyWeapons = {
	
	"fas2_rk95",
	"fas2_ak47",
	"fas2_ak74",
	"fas2_g3",
	"fas2_glock20",
	"fas2_m3s90",
	"fas2_m24",
	"fas2_mp5a5",
	"fas2_pp19",
	"fas2_ragingbull",
	"fas2_rpk",
	"fas2_sg552",
	"fas2_sks",
	"fas2_mac11",
	"fas2_uzi",
	"fas2_famas",
	"fas2_g36c",
	"fas2_m4a1",
	"fas2_m14",
	"fas2_m21",
	"fas2_m82",
	"fas2_sg550",
	"fas2_sr25",
	"fas2_deagle",
	"fas2_m1911",
	"fas2_ots33",
	"fas2_p226",

}

--[[ Property Settings ]]--
GM.Config.GovernemtDoorJobs = { --List of jobs that can lock government doors
	["JOB_POLICE"] = true,
	["JOB_EMS"] = true,
	["JOB_FIREFIGHTER"] = true,
	["JOB_MAYOR"] = true,
	["JOB_SSERVICE"] = true,
}

--[[ Damage Settings ]]--
GM.Config.BleedDamage = 1
GM.Config.BleedInterval = 30
GM.Config.BleedBandageDuration = 60 *2 --Time a bandage should stop bleeding for
GM.Config.ItemDamageTakeCooldown = 45 --Time following a damage event to an item that a player should be blocked from picking the item back up

--[[ Fire System Settings ]]--
GM.Config.MaxFires = 256
GM.Config.MaxChildFires = 25
GM.Config.FireSpreadDistance = 80
GM.Config.FireNodeCount = 4
GM.Config.FireSpreadCount = 2
GM.Config.FireBurnEverything = true
GM.Config.FireSimRate = 6

--[[ Driving Test Questions ]]--
--Note: The questions table must be the same in the shared config, but without the answers!
GM.Config.DrivingTestRetakeDelay = 5 *60
GM.Config.MinCorrectDrivingTestQuestions = 5
GM.Config.DrivingTestQuestions_Answers = {
	{ Question = "What do you do when its a green light?", Options = { ["You begin to move"] = true, ["You stop"] = false, ["You turn off your engine"] = false } },
	{ Question = "What do you do if you see someone thats just crashed.", Options = { ["Continue driving"] = false, ["Call your friends"] = false, ["Investigate the scene"] = true } },
	{ Question = "Someone has just crashed into you and damaged your car.", Options = { ["Pull a weapon on him"] = false, ["Exchange insurance information"] = true, ["Talk shit to him while ramming his car"] = false } },
	{ Question = "Your car seems to be not functioning properly, what do you do?", Options = { ["Call the cops"] = true, ["Stand on the road to get someones attention"] = false, ["Phone up mechanical services"] = false } },
	{ Question = "You encounter a police road block and the officer tells you to turn around, do you", Options = { ["Ignore the officer and continue driving"] = false, ["Sit in your car and do nothing"] = false, ["Carefully turn around and drive"] = true } },
	{ Question = "You see a another driver driving recklessly, what do you do?", Options = { ["Inform the police"] = true, ["Drive recklessly yourself"] = false, ["Message your friend"] = false } },
	{ Question = "You have just accidentally crashed into a pole and you have injured yourself, what do you do?", Options = { ["Lie on the road and wait for someone to help"] = false, ["Follow someone until they help you"] = false, ["Call EMS"] = true } },
}

--[[ NPC Bank Item Storage Settings ]]--
GM.Config.BankStorage_MAX_UNIQUE_ITEMS = 10
GM.Config.BankStorage_MAX_NUM_ITEM = 20
GM.Config.BankStorage_VIP_MAX_UNIQUE_ITEMS = 50
GM.Config.BankStorage_VIP_MAX_NUM_ITEM = 100

--[[ NPC Drug Dealer Settings ]]--
GM.Config.DrugNPCMoveTime_Min = 15 *60
GM.Config.DrugNPCMoveTime_Max = 40 *60

--[[ NPC Jail Warden Settings ]]--
GM.Config.CopLawyerRequestCooldownTime = 60 --Time a player must wait after requesting a lawyer before they may do so again

--[[ Map Settings ]]--
--The smaller the fade min and max are, the sooner map props will stop drawing
GM.Config.DetailPropFadeMin = 1024
GM.Config.DetailPropFadeMax = 1700

--[[ Job Settings ]]--
GM.Config.JobPayInterval = 12 *60 --How often players should get paid
GM.Config.EMSReviveBonus = 100 --How much money to give an EMS player when they revive someone
GM.Config.FireBonus = 100 --How much money to give a firefighter player when they put out enough fires
GM.Config.FireExtinguishBonusCount = 50 --How many fires a player must put out before they get paid the bonus

--[[ Weather ]]--
GM.Config.WeatherRandomizer_MinTime = 60 *4
GM.Config.WeatherRandomizer_MaxTime = 60 *8
GM.Config.WeatherTable = {
	{ ID = "thunder_storm", MinTime = 60 *3.5, MaxTime = 60 *12, Chance = function() return math.random(1, 8) == 1 end },
	{ ID = "thunder_storm", MinTime = 60 *6, MaxTime = 60 *15, Chance = function() return math.random(1, 8) == 1 end },
	{ ID = "light_rain", MinTime = 60 *3.5, MaxTime = 60 *8, Chance = function() return math.random(1, 5) == 1 end },
	{ ID = "light_rain", MinTime = 60 *3.5, MaxTime = 60 *8, Chance = function() return math.random(1, 5) == 1 end },
	{ ID = "light_rain", MinTime = 60 *6, MaxTime = 60 *15, Chance = function() return math.random(1, 10) == 1 end },
}

--[[ Misc Settings ]]--
GM.Config.AdvertPrice = 20
GM.Config.MinDrugConfiscatePrice = 25
GM.Config.MaxDrugConfiscatePrice = 50
GM.Config.DefWalkSpeed = 85
GM.Config.DefRunSpeed = 185
GM.Config.MaxRunSpeed = 260

--[[ Hard Coded Ban List ]]--
GM.Config.Banned4Lyfe = {
--	["76561197964083008"] = "lol", --Fredy, thanks for showing me what was wrong in the end i guess
	["76561198035956018"] = "lol", --Bolli, fredy's crew
	["76561198037374396"] = "lol", --StephenPuffs, fredy's crew
	["76561198162962704"] = "", --Leystryku, okay guy, but still lol, deleted the website
}
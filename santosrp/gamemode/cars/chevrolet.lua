--[[
	Name: chevrolet.lua
	For: TalosLife
	By: TalosLife
]]--

local Car = {}
Car.Make = "Chevrolet"
Car.Name = "Camaro ZL1"
Car.UID = "che_camarozl1tdm"
Car.Desc = "The Chevrolet, gmod-able by TDM"
Car.Model = "models/tdmcars/chev_camzl1.mdl"
Car.Script = "scripts/vehicles/TDMCars/for_crownvic.txt"
Car.Price = 67500
Car.TrunkSize = 500
Car.FuellTank = 150
Car.FuelConsumption = 12.125
Car.LPlates = {

	{

		pos = Vector( 0, -112, 33.2 ),

		ang = Angle( 0, 0, 87 ),

		scale = 0.027

	}

}
GM.Cars:Register( Car )

local Car = {}
Car.VIP = true
Car.Make = "Chevrolet"
Car.Name = "Chevele SS"
Car.UID = "che_chevellesstdm"
Car.Desc = "The Chevrolet, gmod-able by TDM"
Car.Model = "models/tdmcars/chevelless.mdl"
Car.Script = "scripts/vehicles/TDMCars/300c.txt"
Car.TrunkSize = 500
Car.Price = 28000
Car.FuellTank = 150
Car.FuelConsumption = 12.125
Car.LPlates = {

	{

		pos = Vector( 0, 111, 24 ),

		ang = Angle( 0, 180, 90 ),

		scale = 0

	},

	{

		pos = Vector( 0, -121, 25.8 ),

		ang = Angle( 0, 0, 90 ),

		scale = 0.02

	}

}
GM.Cars:Register( Car )

local Car = {}
Car.VIP = true
Car.Make = "Chevrolet"
Car.Name = "Corvette GSC"
Car.UID = "che_corv_gsctdm"
Car.Desc = "The Chevrolet, gmod-able by TDM"
Car.Model = "models/tdmcars/chev_corv_gsc.mdl"
Car.Script = "scripts/vehicles/TDMCars/300c.txt"
Car.TrunkSize = 500
Car.Price = 45000
Car.FuellTank = 150
Car.FuelConsumption = 12.125
Car.LPlates = {

	{

		pos = Vector( 0, 108.5, 15 ),

		ang = Angle( 0, 180, 90 ),

		scale = 0.022

	},

	{

		pos = Vector( 0, -103, 29.8 ),

		ang = Angle( 0, 0, 80 ),

		scale = 0.028

	}

}
GM.Cars:Register( Car )

local Car = {}
Car.VIP = true
Car.Make = "Chevrolet"
Car.Name = "Corvette Stingray"
Car.UID = "che_stingray427tdm"
Car.Desc = "The Chevrolet, gmod-able by TDM"
Car.Model = "models/tdmcars/chev_stingray427.mdl"
Car.Script = "scripts/vehicles/TDMCars/300c.txt"
Car.TrunkSize = 500
Car.Price = 32000
Car.FuellTank = 150
Car.FuelConsumption = 12.125
Car.LPlates = {

	{
		pos = Vector( 0, -104, 25.7 ),
		ang = Angle( 0, 0, 90 ),
		scale = 0.031
	}

}
GM.Cars:Register( Car )

local Car = {}
Car.Make = "Chevrolet"
Car.Name = "Impala SS"
Car.UID = "che_impala96tdm"
Car.Desc = "The Chevrolet, gmod-able by TDM"
Car.Model = "models/tdmcars/chev_impala96.mdl"
Car.Script = "scripts/vehicles/TDMCars/beetle68.txt"
Car.TrunkSize = 500
Car.Price = 6000
Car.FuellTank = 150
Car.FuelConsumption = 12.125
Car.LPlates = {

	{

		pos = Vector( 0, 121, 19 ),

		ang = Angle( 0, 180, 90 ),

		scale = 0.026

	},

	{

		pos = Vector( 0, -139, 34.5 ),

		ang = Angle( 0, 0, 75 ),

		scale = 0.029

	}

}
GM.Cars:Register( Car )

local Car = {}
Car.Make = "Chevrolet"
Car.Name = "Spark"
Car.UID = "che_sparktdm"
Car.Desc = "The Chevrolet, gmod-able by TDM"
Car.Model = "models/tdmcars/spark.mdl"
Car.Script = "scripts/vehicles/TDMCars/beetle68.txt"
Car.TrunkSize = 500
Car.Price = 15000
Car.FuellTank = 150
Car.FuelConsumption = 12.125
Car.LPlates = {

	{

		pos = Vector( 0, 88.25, 20 ),

		ang = Angle( 0, 180, 90 ),

		scale = 0.035

	},

	{

		pos = Vector( 0, -85.5, 39 ),

		ang = Angle( 0, 0, 70 ),

		scale = 0.035

	}	

}
GM.Cars:Register( Car )
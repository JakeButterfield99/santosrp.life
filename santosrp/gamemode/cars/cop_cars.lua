--[[
	Name: cop_cars.lua
	For: SantosRP
	By: SantosRP
]]--

local Car = {}
Car.Make = "Dodge"
Car.Name = "SRT8 Police"
Car.UID = "srt8_police"
Car.Job = "JOB_POLICE"
Car.Desc = ""
Car.Model = "models/tdmcars/emergency/chargersrt8.mdl"
Car.Script = "scripts/vehicles/TDMCars/chargersrt8.txt"
Car.TrunkSize = 300
Car.FuellTank = 120
Car.FuelConsumption = 20
Car.LPlates = {

	{
		pos = Vector( 0, -120, 29.5 ),
		ang = Angle( 0, 0, 90 ),
		scale = 0.028
	},
	{
		pos = Vector( 0, 122, 23 ),
		ang = Angle( 0, 180, 90 ),
		scale = 0.028
	}

}

GM.Cars:RegisterJobCar( Car )

local Car = {}
Car.Make = "Dodge"
Car.Name = "2015 Charger Marked"
Car.UID = "2015_charger_marked"
Car.Job = "JOB_POLICE"
Car.Desc = ""
Car.Model = "models/lonewolfie/dodge_charger_2015_police.mdl"
Car.Script = "scripts/vehicles/lwcars/dodge_charger_2015_police.txt"
Car.TrunkSize = 300
Car.FuellTank = 120
Car.FuelConsumption = 20
Car.LPlates = {

	{
		pos = Vector( 0, -127, 29.5 ),
		ang = Angle( 0, 0, 90 ),
		scale = 0.028
	},
	{
		pos = Vector( 0, 125, 20 ),
		ang = Angle( 0, 180, 90 ),
		scale = 0.028
	}

}
GM.Cars:RegisterJobCar( Car )

local Car = {}
Car.Make = "Dodge"
Car.Name = "2015 Charger Unmarked"
Car.UID = "2015_charger_unmarked"
Car.Job = "JOB_POLICE"
Car.Desc = ""
Car.Model = "models/lonewolfie/dodge_charger_2015_undercover.mdl"
Car.Script = "scripts/vehicles/lwcars/dodge_charger_2015_police.txt"
Car.TrunkSize = 300
Car.FuellTank = 120
Car.FuelConsumption = 20
Car.LPlates = {

	{
		pos = Vector( 0, -127, 29.5 ),
		ang = Angle( 0, 0, 90 ),
		scale = 0.028
	},
	{
		pos = Vector( 0, 125, 20 ),
		ang = Angle( 0, 180, 90 ),
		scale = 0.028
	}

}
GM.Cars:RegisterJobCar( Car )

local Car = {}
Car.Make = "Dodge"
Car.Name = "SRT8 2012 Police"
Car.UID = "srt8_2012_police"
Car.Job = "JOB_POLICE"
Car.Desc = ""
Car.Model = "models/tdmcars/emergency/dod_charger12.mdl"
Car.Script = "scripts/vehicles/TDMCars/charger2012.txt"
Car.TrunkSize = 300
Car.FuellTank = 120
Car.FuelConsumption = 20
Car.LPlates = {

	{

		pos = Vector( 0, 121, 23 ),

		ang = Angle( 0, 180, 90 ),

		scale = 0.022

	},

	{

		pos = Vector( 0, -126, 31.5 ),

		ang = Angle( 0, 0, 85 ),

		scale = 0.022

	}

}
GM.Cars:RegisterJobCar( Car )

local Car = {}
Car.Make = "Ford"
Car.Name = "Crown Victoria"
Car.UID = "crown_vic"
Car.Job = "JOB_POLICE"
Car.Desc = ""
Car.Model = "models/tdmcars/emergency/for_crownvic.mdl"
Car.Script = "scripts/vehicles/TDMCars/for_crownvic.txt"
Car.TrunkSize = 300
Car.FuellTank = 120
Car.FuelConsumption = 20
Car.LPlates = {

	{

		pos = Vector( 0, 127.5, 22.5 ),

		ang = Angle( 0, 180, 100 ),

		scale = 0.018

	},

	{

		pos = Vector( 0, -123.5, 38.8 ),

		ang = Angle( 0, 0, 80 ),

		scale = 0.025

	}

}
GM.Cars:RegisterJobCar( Car )

local Car = {}
Car.Make = "Ford"
Car.Name = "Taurus 2013"
Car.UID = "taurus2013"
Car.Job = "JOB_POLICE"
Car.Desc = ""
Car.Model = "models/tdmcars/emergency/for_taurus_13.mdl"
Car.Script = "scripts/vehicles/TDMCars/for_taurus_13.txt"
Car.TrunkSize = 300
Car.FuellTank = 120
Car.FuelConsumption = 20
Car.LPlates = {

	{

		pos = Vector( 0, -123.3, 26 ),

		ang = Angle( 0, 0, 86.5 ),

		scale = 0.026

	}

}
GM.Cars:RegisterJobCar( Car )

local Car = {}
Car.Make = "Chevrolet"
Car.Name = "Suburban"
Car.UID = "police_suburban"
Car.Job = "JOB_POLICE"
Car.Desc = "A tahoe."
Car.Model = "models/lonewolfie/chev_suburban_pol.mdl"
Car.Script = "scripts/vehicles/lwcars/chev_suburban.txt"
Car.TrunkSize = 300
Car.FuellTank = 120
Car.FuelConsumption = 20
Car.LPlates = {

	{

		pos = Vector( 0, 136.2, 22.5 ),

		ang = Angle( 0, 180, 117 ),

		scale = 0.030

	},

	{

		pos = Vector( 0, -129.5, 46.6 ),

		ang = Angle( 0, 0, 90 ),

		scale = 0.029

	}

}

GM.Cars:RegisterJobCar( Car )

local Car = {}
Car.Make = "Chevrolet"
Car.Name = "Suburban Undercover"
Car.UID = "police_suburban_undercover"
Car.Job = "JOB_POLICE"
Car.Desc = "A Suburban undercover."
Car.Model = "models/lonewolfie/chev_suburban_pol_und.mdl"
Car.Script = "scripts/vehicles/lwcars/chev_suburban.txt"
Car.TrunkSize = 300
Car.FuellTank = 120
Car.FuelConsumption = 20
Car.LPlates = {

	{

		pos = Vector( 0, 136.2, 22.5 ),

		ang = Angle( 0, 180, 117 ),

		scale = 0.030

	},

	{

		pos = Vector( 0, -129.5, 46.6 ),

		ang = Angle( 0, 0, 90 ),

		scale = 0.029

	}

}

GM.Cars:RegisterJobCar( Car )

local Car = {}
Car.Make = "SWAT"
Car.Name = "SWAT Van"
Car.UID = "vswat"
Car.Job = "JOB_POLICE"
Car.Desc = ""
Car.Model = "models/sentry/swatvan.mdl"
Car.Script = "scripts/vehicles/sentry/vswat.txt"
Car.TrunkSize = 800
Car.FuellTank = 120
Car.FuelConsumption = 20
Car.LPlates = {

	{
		pos = Vector( 0, -127, 29.5 ),
		ang = Angle( 0, 0, 90 ),
		scale = 0.028
	},
	{
		pos = Vector( 0, 107, 29 ),
		ang = Angle( 0, 180, 90 ),
		scale = 0.028
	}

}
GM.Cars:RegisterJobCar( Car )

local Car = {}
Car.Make = "Chevrolet"
Car.Name = "Tahoe"
Car.UID = "police_tahoe"
Car.Job = "JOB_POLICE"
Car.Desc = ""
Car.Model = "models/lonewolfie/chev_tahoe_police.mdl"
Car.Script = "scripts/vehicles/lwcars/chev_tahoe.txt"
Car.TrunkSize = 300
Car.FuellTank = 120
Car.FuelConsumption = 20
Car.LPlates = {

	{

		pos = Vector( 0, 114.5, 20 ),

		ang = Angle( 0, 180, 106 ),

		scale = 0.028

	},

	{

		pos = Vector( 0, -113, 47 ),

		ang = Angle( 0, 0, 83.5 ),

		scale = 0.029

	}

}
GM.Cars:RegisterJobCar( Car )

local Car = {}
Car.Make = "Jaguar"
Car.Name = "XFR"
Car.UID = "jag_xfr_pol"
Car.Job = "JOB_POLICE"
Car.Desc = ""
Car.Model = "models/lonewolfie/jaguar_xfr_pol.mdl"
Car.Script = "scripts/vehicles/lwcars/jag_xfr_pol.txt"
Car.FuellTank = 120
Car.FuelConsumption = 20
GM.Cars:RegisterJobCar( Car )

local Car = {}
Car.Make = "Jaguar"
Car.Name = "XFR Special Escort Group"
Car.UID = "jag_xfr_pol_und"
Car.Job = "JOB_POLICE"
Car.Desc = ""
Car.Model = "models/lonewolfie/jaguar_xfr_pol_und.mdl"
Car.Script = "scripts/vehicles/lwcars/jag_xfr_pol.txt"
Car.FuellTank = 120
Car.FuelConsumption = 20
GM.Cars:RegisterJobCar( Car )
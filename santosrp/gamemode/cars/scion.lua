--[[
	Name: scion.lua
	For: TalosLife
	By: TalosLife
]]--

local Car = {}
Car.Make = "Scion"
Car.Name = "xD"
Car.UID = "scionxdtdm"
Car.Desc = "The Scion xD, gmod-able by TDM"
Car.Model = "models/tdmcars/scion_xd.mdl"
Car.Script = "scripts/vehicles/TDMCars/beetle68.txt"
Car.TrunkSize = 500
Car.Price = 18000
Car.FuellTank = 150
Car.FuelConsumption = 12.125
Car.LPlates = {

	{

		pos = Vector( 0, 89.3, 23.5 ),

		ang = Angle( 0, 180, 85 ),

		scale = 0.025

	},

	{

		pos = Vector( 0, -87.5, 37.1 ),

		ang = Angle( 0, 0, 80 ),

		scale = 0.029

	}

}
GM.Cars:Register( Car )

local Car = {}
Car.Make = "Scion"
Car.Name = "FR-s"
Car.UID = "scionfrstdm"
Car.Desc = "The Scion xD, gmod-able by TDM"
Car.Model = "models/tdmcars/scion_frs.mdl"
Car.Script = "scripts/vehicles/TDMCars/for_crownvic.txt"
Car.TrunkSize = 500
Car.Price = 26500
Car.FuellTank = 150
Car.FuelConsumption = 12.125
Car.LPlates = {

	{

		pos = Vector( 0, 102.9, 21.5 ),

		ang = Angle( 0, 180, 85 ),

		scale = 0.025

	},

	{

		pos = Vector( 0, -100.6, 39.9 ),

		ang = Angle( 0, 0, 90 ),

		scale = 0.030

	}

}
GM.Cars:Register( Car )

local Car = {}
Car.Make = "Scion"
Car.Name = "tC"
Car.UID = "sciontctdm"
Car.Desc = "The Scion xD, gmod-able by TDM"
Car.Model = "models/tdmcars/scion_tc.mdl"
Car.Script = "scripts/vehicles/TDMCars/300c.txt"
Car.TrunkSize = 500
Car.Price = 19500
Car.FuellTank = 150
Car.FuelConsumption = 12.125
Car.LPlates = {

	{

		pos = Vector( 0, 105.9, 21.5 ),

		ang = Angle( 0, 180, 85 ),

		scale = 0.025

	},

	{

		pos = Vector( 0, -99.3, 36.1 ),

		ang = Angle( 0, 0, 80 ),

		scale = 0.025

	}

}
GM.Cars:Register( Car )
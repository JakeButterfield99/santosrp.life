--[[
	Name: mitsubishi.lua
	For: TalosLife
	By: Bradley
]]--

local Car = {}
Car.Make = "Mitsubishi"
Car.Name = "Mitsubishi Lancer Evolution X GSR"
Car.UID = "mitsu_evoxtdm"
Car.Desc = "A drivable Mitsubishi Lancer Evolution VIII by TheDanishMaster"
Car.Model = "models/tdmcars/mitsu_evox.mdl"
Car.Script = "scripts/vehicles/TDMCars/mitsu_evox.txt"
Car.Price = 34000
Car.TrunkSize = 700
Car.FuellTank = 59
Car.FuelConsumption = 15.6
Car.LPlates = {

	{

		pos = Vector( -22, 47, 53 ),

		ang = Angle( 0, 195, 25 ),

		scale = 0.028

	},

	{

		pos = Vector( 0, -107.75, 27 ),

		ang = Angle( 0, 0, 98 ),

		scale = 0.026

	}

}
GM.Cars:Register( Car )

local Car = {}
Car.Make = "Mitsubishi"
Car.Name = "Lancer Evolution VIII"
Car.UID = "mitsubishi_lancer_evolution"
Car.Desc = "A drivable Mitsubishi Lancer Evolution VIII by TheDanishMaster"
Car.Model = "models/tdmcars/mitsu_evo8.mdl"
Car.Script = "scripts/vehicles/TDMCars/mitsu_evo8.txt"
Car.Price = 23000
Car.TrunkSize = 700
Car.FuellTank = 59
Car.FuelConsumption = 15.6
Car.LPlates = {

	{

		pos = Vector( 0, 102.2, 15.7 ),

		ang = Angle( 0, 180, 90 ),

		scale = 0.022

	},

	{

		pos = Vector( 0, -103, 21 ),

		ang = Angle( 0, 0, 75 ),

		scale = 0.022

	}

}
GM.Cars:Register( Car )

local Car = {}
Car.Make = "Mitsubishi"
Car.Name = "Eclipse GT"
Car.UID = "mitsubishi_eclipse_gt"
Car.Desc = "A drivable Mitsubishi Eclipse GT by TheDanishMaster"
Car.Model = "models/tdmcars/mitsu_eclipgt.mdl"
Car.Script = "scripts/vehicles/TDMCars/mitsu_eclipgt.txt"
Car.Price = 13000
Car.TrunkSize = 500
Car.FuellTank = 79
Car.FuelConsumption = 15.6
Car.LPlates = {

	{
		pos = Vector( 0, 109, 23 ),
		ang = Angle( 0, 180, 90 ),
		scale = 0.028
	},
	{
		pos = Vector( 0, -103, 30 ),
		ang = Angle( 0, 0, 75 ),
		scale = 0.030
	}

}
GM.Cars:Register( Car )

local Car = {}
Car.Make = "Mitsubishi"
Car.Name = "Colt Ralliart"
Car.UID = "colttdm"
Car.Desc = "A drivable Mitsubishi Colt Ralliart by TheDanishMaster"
Car.Model = "models/tdmcars/coltralliart.mdl"
Car.Script = "scripts/vehicles/TDMCars/colt.txt"
Car.Price = 6000
Car.TrunkSize = 300
Car.FuellTank = 45
Car.FuelConsumption = 16
Car.LPlates = {

	{
		pos = Vector( 0, 102.5, 25 ),
		ang = Angle( 0, 180, 90 ),
		scale = 0.028
	},
	{
		pos = Vector( 0, -90, 22 ),
		ang = Angle( 0, 0, 80 ),
		scale = 0.030
	}

}
GM.Cars:Register( Car )

local Car = {}
Car.Make = "Mitsubishi"
Car.Name = "Mitsubishi Eclipse GSX"
Car.UID = "mitsubishi_gsx_esclp"
Car.Desc = "A drivable Mitsubishi Esclipse GSX by TheDanishMaster"
Car.Model = "models/tdmcars/mit_eclipsegsx.mdl"
Car.Script = "scripts/vehicles/TDMCars/mit_eclipsegsx.txt"
Car.Price = 22000
Car.TrunkSize = 500
Car.FuellTank = 45
Car.FuelConsumption = 16
Car.LPlates = {

	{
		pos = Vector( 0, 103, 21 ),
		ang = Angle( 0, 180, 90 ),
		scale = 0.028
	},
	{
		pos = Vector( 0, -100, 30 ),
		ang = Angle( 0, 0, 75 ),
		scale = 0.030
	}

}
GM.Cars:Register( Car )
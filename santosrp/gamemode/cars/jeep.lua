--[[
	Name: jeep.lua
	For: TalosLife
	By: Bradley
]]--

local Car = {}
Car.Make = "Jeep"
Car.Name = "Jeep Grand Cherokee 2012"
Car.UID = "jeep_grand"
Car.Desc = "A drivable Jeep by TheDanishMaster"
Car.Model = "models/tdmcars/jeep_grandche.mdl"
Car.Script = "scripts/vehicles/TDMCars/grandche.txt"
Car.Price = 26000
Car.TrunkSize = 1200
Car.FuellTank = 80
Car.FuelConsumption = 14.375
Car.LPlates = {

	{

		pos = Vector( 0, 120, 30 ),

		ang = Angle( 0, 180, 90 ),

		scale = 0.026

	},

	{

		pos = Vector( 0, -114, 52 ),

		ang = Angle( 0, 0, 90 ),

		scale = 0.029

	}

}
GM.Cars:Register( Car )

local Car = {}
Car.Make = "Jeep"
Car.Name = "Jeep Wrangler"
Car.UID = "jeep_wrang"
Car.Desc = "A drivable Jeep by TheDanishMaster"
Car.Model = "models/tdmcars/wrangler.mdl"
Car.Script = "scripts/vehicles/TDMCars/wrangler.txt"
Car.Price = 27000
Car.TrunkSize = 700
Car.FuellTank = 80
Car.FuelConsumption = 8
Car.LPlates = {

	{

		pos = Vector( 0, 91, 32.4 ),

		ang = Angle( 0, 180, 90 ),

		scale = 0.025

	},

	{

		pos = Vector( -32, -79.5, 39 ),

		ang = Angle( 0, 0, 90 ),

		scale = 0.025

	}

}
GM.Cars:Register( Car )
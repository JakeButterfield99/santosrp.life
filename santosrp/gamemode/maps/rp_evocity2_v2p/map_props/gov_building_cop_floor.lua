--[[
	Name: gov_building_cop_floor.lua
	For: TalosLife
	By: TalosLife
]]--

local MapProp = {}
MapProp.ID = "gov_building_cop_floor"
MapProp.m_tblSpawn = {
	{ mdl = 'models/props/cs_office/phone.mdl',pos = Vector('-150.013824 -1326.397217 499.653198'), ang = Angle('0.000 -75.575 -0.406') },
	{ mdl = 'models/props/cs_office/microwave.mdl',pos = Vector('187.273102 -1603.280151 505.556702'), ang = Angle('0.005 92.098 -0.071') },
	{ mdl = 'models/props/cs_office/chair_office.mdl',pos = Vector('-92.269699 -1373.395386 468.288086'), ang = Angle('0.000 112.500 0.000') },
	{ mdl = 'models/props/cs_office/computer.mdl',pos = Vector('-95.899956 -1321.096313 499.734985'), ang = Angle('0.099 -112.423 -0.038') },
	{ mdl = 'models/props/cs_office/chair_office.mdl',pos = Vector('-536.809753 -1609.932861 468.345520'), ang = Angle('-0.011 -91.258 0.011') },
	{ mdl = 'models/props_combine/breendesk.mdl',pos = Vector('-111.647522 -1318.958618 468.225281'), ang = Angle('0.000 90.000 0.000') },
	{ mdl = 'models/props/cs_office/plant01.mdl',pos = Vector('-806.272583 -1486.196167 467.781097'), ang = Angle('0.000 -44.006 0.000') },
	{ mdl = 'models/props_combine/breenchair.mdl',pos = Vector('-787.022522 -1694.032104 468.389008'), ang = Angle('-0.401 3.356 -0.137') },
	{ mdl = 'models/props/cs_office/shelves_metal3.mdl',pos = Vector('185.494415 -2189.437500 468.467926'), ang = Angle('0.000 0.000 0.000') },
	{ mdl = 'models/props_interiors/furniture_couch02a.mdl',pos = Vector('-118.455132 -2387.583496 489.910767'), ang = Angle('0.401 136.961 0.000') },
	{ mdl = 'models/props_c17/bench01a.mdl',pos = Vector('-299.688629 -1585.218872 487.867340'), ang = Angle('0.044 -0.005 -0.005') },
	{ mdl = 'models/props/cs_office/phone.mdl',pos = Vector('-728.465332 -1720.003540 498.825134'), ang = Angle('0.170 136.692 0.055') },
	{ mdl = 'models/props/cs_office/chair_office.mdl',pos = Vector('-616.257141 -1601.017578 468.336731'), ang = Angle('-0.016 -74.883 -0.011') },
	{ mdl = 'models/props/cs_office/paperbox_pile_01.mdl',pos = Vector('171.590118 -2304.323730 468.312134'), ang = Angle('0.011 -179.989 0.000') },
	{ mdl = 'models/props/cs_office/chair_office.mdl',pos = Vector('-531.663635 -1780.667358 468.499786'), ang = Angle('0.000 59.392 0.000') },
	{ mdl = 'models/props/cs_office/plant01.mdl',pos = Vector('-293.866455 -1078.783813 467.753357'), ang = Angle('0.055 -22.505 -0.099') },
	{ mdl = 'models/props_interiors/furniture_couch02a.mdl',pos = Vector('-264.992737 -2393.550537 489.822693'), ang = Angle('-0.093 45.000 0.022') },
	{ mdl = 'models/props/cs_office/sofa_chair.mdl',pos = Vector('-107.527473 -1117.949219 468.360779'), ang = Angle('0.000 -134.621 0.055') },
	{ mdl = 'models/props/cs_office/chair_office.mdl',pos = Vector('-619.511963 -1776.397339 468.400055'), ang = Angle('-0.198 90.961 0.269') },
	{ mdl = 'models/props_c17/furnituretable002a.mdl',pos = Vector('179.782944 -1620.298584 486.523926'), ang = Angle('0.066 -179.951 -0.016') },
	{ mdl = 'models/props/cs_office/file_cabinet1_group.mdl',pos = Vector('-297.907318 -1351.406372 468.366882'), ang = Angle('0.000 -0.011 0.016') },
	{ mdl = 'models/props/cs_office/table_coffee.mdl',pos = Vector('-204.517929 -1152.234253 468.445496'), ang = Angle('0.198 -92.477 0.000') },
	{ mdl = 'models/props/cs_office/table_coffee.mdl',pos = Vector('-191.800491 -2354.690186 468.425903'), ang = Angle('-0.220 89.539 0.016') },
	{ mdl = 'models/props/cs_office/table_meeting.mdl',pos = Vector('-615.955383 -1694.262817 468.414764'), ang = Angle('0.000 0.000 0.000') },
	{ mdl = 'models/props/cs_office/chair_office.mdl',pos = Vector('-692.885193 -1773.195068 468.499786'), ang = Angle('0.000 91.005 0.000') },
	{ mdl = 'models/props_interiors/furniture_couch02a.mdl',pos = Vector('-191.129150 -2419.888184 489.850189'), ang = Angle('-0.016 89.907 -0.038') },
	{ mdl = 'models/props/cs_office/paper_towels.mdl',pos = Vector('194.402374 -1646.061523 514.665222'), ang = Angle('88.885 -6.575 160.071') },
	{ mdl = 'models/props/cs_office/sofa.mdl',pos = Vector('-203.704498 -1085.198364 468.354706'), ang = Angle('0.000 -90.000 0.000') },
	{ mdl = 'models/props/cs_office/offinspb.mdl',pos = Vector('-311.549103 -1416.175781 560.763000'), ang = Angle('0.000 -89.995 0.000') },
	{ mdl = 'models/props/cs_office/trash_can.mdl',pos = Vector('-170.276733 -1316.247070 468.499786'), ang = Angle('0.000 89.901 0.000') },
	{ mdl = 'models/props_lab/cactus.mdl',pos = Vector('-148.235794 -1308.959106 504.709076'), ang = Angle('0.000 62.271 0.000') },
	{ mdl = 'models/props/cs_office/chair_office.mdl',pos = Vector('-693.640808 -1609.439087 468.499786'), ang = Angle('0.000 -106.024 0.000') },
	{ mdl = 'models/props/cs_office/file_cabinet1_group.mdl',pos = Vector('-297.738800 -1415.691162 468.487000'), ang = Angle('-0.033 -0.016 0.005') },
}

GAMEMODE.Map:RegisterMapProp( MapProp )
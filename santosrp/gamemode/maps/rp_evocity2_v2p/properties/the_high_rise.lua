local Prop = {}
Prop.Name = "The High Rise"
Prop.Cat = "Stores"
Prop.Price = 800
Prop.Doors = {
	Vector( -2436, -2698.5, 139 ),
	Vector( -2496, -2699, 139 ),
}

GM.Property:Register( Prop )
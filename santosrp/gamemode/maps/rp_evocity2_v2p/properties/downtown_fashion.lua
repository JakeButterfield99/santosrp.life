local Prop = {}
Prop.Name = "Downtown Fashion"
Prop.Government = true
Prop.Doors = {
	Vector( -2438, -1653.5, 139 ),
	Vector( -2498, -1654, 139 ),
	Vector( -2786, -1353, 130.25 ),
}

GM.Property:Register( Prop )
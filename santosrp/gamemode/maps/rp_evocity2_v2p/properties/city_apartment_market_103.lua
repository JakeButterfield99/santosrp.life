local Prop = {}
Prop.Name = "City Apartment Market 103"
Prop.Cat = "Stores"
Prop.Price = 800
Prop.Doors = {
	Vector( 3620, -2891, 130.25 ),
	Vector( 4155, -2756, 130.25 ),
}

GM.Property:Register( Prop )
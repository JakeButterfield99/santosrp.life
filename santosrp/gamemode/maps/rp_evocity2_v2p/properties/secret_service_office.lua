local Prop = {}
Prop.Name = "Secret Service Office"
Prop.Government = true
Prop.Doors = {
	Vector( 14, -1452, 3849.25 ),
	Vector( -126, -1452, 3849.25 ),
}

GM.Property:Register( Prop )
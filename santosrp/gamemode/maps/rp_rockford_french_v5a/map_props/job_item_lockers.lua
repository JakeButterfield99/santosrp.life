--[[
	Name: job_item_lockers.lua
	For: SantosRP
	By: Ultra
]]--

local MapProp = {}
MapProp.ID = "job_item_lockers"
MapProp.m_tblSpawn = {}
MapProp.m_tblEnts = {
	{ pos = Vector(203.459335, 8784.578125, 641), ang = Angle(0, -90, 0), model = "models/props_c17/lockers001a.mdl", job = "JOB_EMS" },
	{ pos = Vector(-5097.235352, -9037.344727, 750), ang = Angle(0, 90, 0), model = "models/props_c17/lockers001a.mdl", job = "JOB_SSERVICE" },
	{ pos = Vector(1635.323486, 3915.032959, 566), ang = Angle(0, 90, 0), model = "models/props_c17/lockers001a.mdl", job = "JOB_FIREFIGHTER" },
	{ pos = Vector(-7895.406738, -5186.546875, 34), ang = Angle(0, -180, 0), model = "models/props_c17/lockers001a.mdl", job = "JOB_POLICE" },
}

function MapProp:CustomSpawn()
	for _, propData in pairs( self.m_tblEnts ) do
		local ent = ents.Create( "ent_job_item_locker" )
		ent:SetPos( propData.pos )
		ent:SetAngles( propData.ang )
		ent:SetCollisionGroup( COLLISION_GROUP_NONE )
		ent:SetMoveType( MOVETYPE_NONE )
		ent.IsMapProp = true
		ent.MapPropID = id
		ent:Spawn()
		ent:Activate()
		ent:SetModel( propData.model )
		ent:SetJobID( _G[propData.job] )

		local phys = ent:GetPhysicsObject()
		if IsValid( phys ) then
			phys:EnableMotion( false )
		end
	end
end

GAMEMODE.Map:RegisterMapProp( MapProp )
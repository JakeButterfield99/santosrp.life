--[[
	Name: mayors_office.lua
	For: TalosLife
	By: TalosLife
]]--

local MapProp = {}
MapProp.ID = "mayors_office"
MapProp.m_tblSpawn = {}
MapProp.m_tblComputers = {
	//{ class = "ent_computer_law", job = {"JOB_LAWYER", "JOB_PROSECUTOR"}, pos = Vector('-8672.583008 10500.851563 320.00'), ang = Angle('-0.000 180.000 -0.000'), },
	//{ class = "ent_computer_law", job = {"JOB_LAWYER", "JOB_PROSECUTOR"}, pos = Vector('-5604.746094 12561.255859 869.00'), ang = Angle('0.000 180.000 0.000'), },
	{ class = "ent_computer_mayor", job = {"JOB_MAYOR"}, pos = Vector('-3734.527344 -7883.047363 758'), ang = Angle('3.485008 114.652039 0.000000'), },
	//{ class = "ent_computer_judge", job = {"JOB_JUDGE"}, pos = Vector('-6091.372070 12294.354492 863.00'), ang = Angle('0.000 00.00 00.000'), },
}

function MapProp:CustomSpawn()
	for _, propData in pairs( self.m_tblComputers ) do
		local ent = ents.Create( propData.class or "ent_computer_base" )
		ent:SetPos( propData.pos )
		ent:SetAngles( propData.ang )
		ent.IsMapProp = true
		ent:SetJobsRequired( propData.job )
		ent:Spawn()
		ent:Activate()

		local phys = ent:GetPhysicsObject()
		if IsValid( phys ) then
			phys:EnableMotion( false )
		end
	end
end


GAMEMODE.Map:RegisterMapProp( MapProp )
--[[
	Name: job_config.lua
	For: TalosLife
	By: TalosLife
]]--

--[[ EMS Config ]]--
if SERVER then
	GM.Config.EMSParkingZone = {
		Min = Vector( -2413.155518, 8012.353027, 427.208252 ),
		Max = Vector( -1168.737793, 9145.905273, 1113.915405 ),
	}
	

	GM.Config.EMSCarSpawns = {
		{ Vector( -1983.001709, 8913.974609, 611.031250 ), Angle( 1.365113, -90.353691, 0.000000 ) },
		{ Vector( -1827.200439, 8913.023438, 611.031250 ), Angle( 1.365113, -90.353691, 0.000000 ) },
		{ Vector( -1671.401001, 8912.069336, 611.031250 ), Angle( 1.365113, -90.353691, 0.000000 ) },
		{ Vector( -1516.538330, 8911.125000, 611.031250 ), Angle( 1.365113, -90.353691, 0.000000 ) },
		{ Vector( -1362.045288, 8910.181641, 611.031250 ), Angle( 1.365113, -90.353691, 0.000000 ) },
		{ Vector( -1348.375122, 8165.407227, 611.031250 ), Angle( 0.321215, 87.766716, 0.000000 ) },
		{ Vector( -1514.948608, 8171.908203, 611.031250 ), Angle( 0.321215, 87.766716, 0.000000 ) },
		{ Vector( -1674.117798, 8178.120117, 611.031250 ), Angle( 0.321215, 87.766716, 0.000000 ) },
		{ Vector( -1822.180542, 8183.898438, 611.031250 ), Angle( 0.321215, 87.766716, 0.000000 ) },
		{ Vector( -1988.897705, 8190.405273, 610.948242 ), Angle( 0.321215, 87.766716, .000000 ) },
	}
end

--[[ Fire Config ]]--
if SERVER then
	GM.Config.FireParkingZone = {
		Min = Vector( 1812.988770, 3937.418945, 499.356049 ),
		Max = Vector( 2473.279297, 4677.401855, 987.542358 ),
	}
	
	

	GM.Config.FireCarSpawns = {
		{ Vector( 2211.588379, 4517.111328, 600.031250 ), Angle( 2.838100, -90, 0.000000 ) },
		{ Vector( 2211.361328, 4336.678711, 600.031250 ), Angle( 2.838100, -90, 0.000000 ) },
		{ Vector( 2211.109863, 4138.150879, 600.031250 ), Angle( 2.838100, -90, 0.000000 ) },
	}
	
	
	GM.Config.FireStationGarageDoors = {
        Vector( 2439.049316, 4526.585449, 600.031250 ),
        Vector( 2441.297119, 4344.121094, 600.031250 ),
        Vector( 2439.140381, 4169.350586, 600.031250 ),
	}

end

--[[ Bank Door Config]]--
if SERVER then
	GM.Config.PrivateBankDoors = {
		Vector( -5225.197754, -5845.977051, 72.031258 ),
	}
end

--[[ Cop Config ]]--
if SERVER then
	GM.Config.CopParkingZone = {
		Min = Vector( -8730.693359, -6471.601074, -92.325928 ),
		Max = Vector( -7535.288086, -5341.665527, 692.589294 ),
	}

	GM.Config.CopCarSpawns = {
		{ Vector( -7928.678711, -5485.957031, 64.031250 ), Angle( 5.407718, -180, 0.000000 ) },
		{ Vector( -8075.086914, -5481.938965, 64.031250 ), Angle( 5.407718, -180, 0.000000 ) },
		{ Vector( -8239.601563, -5477.424316, 64.031250 ), Angle( 5.407718, -180, 0.000000 ) },
		{ Vector( -8546.956055, -6078.435059, 64.031250 ), Angle( 3.721420, -90, 0.000000 ) },
		{ Vector( -8560.873047, -6243.933105, 64.031250 ), Angle( 3.721420, -90, 0.000000 ) },
	}
end

--[[ Tow Config ]]--
if SERVER then
	GM.Config.TowWelderZone = {
		Min = Vector( -7726.078125, 149.787323, -51.830681 ),
		Max = Vector( -6998.450195, 638.315857, 503.850220 ),
	}

	GM.Config.TowParkingZone = {
		Min = Vector( -7869.998535, 631.641724, -67.006279 ),
		Max = Vector( -7112.576660, 1899.250732, 411.691406 ),
	}

	GM.Config.TowCarSpawns = {
		{ Vector( -7496.375488, 1756.916138, 65.031250 ), Angle( 0, -90, 0 ) },
		{ Vector( -7516.022949, 1091.180664, 65.031250 ), Angle( 0, -90, 0 ) },
	}
end

--[[ Taxi Config ]]--
if SERVER then
	GM.Config.TaxiParkingZone = {
		Min = Vector( 1423.643677, 2539.537354, 421.563263 ),
		Max = Vector( 2020.789795, 3174.026611, 797.808105 ),
	}

	GM.Config.TaxiCarSpawns = {
		{ Vector( 1547.096680, 2698.384766, 600.031250 ), Angle( 0, 90, 0 ) },
		{ Vector( 1714.235474, 2697.809814, 600.031250 ), Angle( 0, 90, 0 ) },
		{ Vector( 1869.781372, 2697.275146, 600.031250 ), Angle( 0, 90, 0 ) },
	}
end

--[[ Sales Config ]]--
if SERVER then
	GM.Config.SalesParkingZone = {
		Min = Vector( -8939.512695, 5335.324219, -116.413315 ),
		Max = Vector( -7024.569824, 7124.114258, 375.322479 ),
	}

	GM.Config.SalesCarSpawns = {
		{ Vector( -7178.345215, 5495.161621, 64.031250 ), Angle( 0.910896, 89.019722, 0.000000 ) },
		{ Vector( -7341.824219, 5497.960449, 64.031250 ), Angle( 0.910896, 89.019722, 0.000000 ) },
		{ Vector( -7499.407715, 5500.658691, 64.031250 ), Angle( 0.910896, 89.019722, 0.000000 ) },
		{ Vector( -7659.124512, 5503.394043, 64.031250 ), Angle( 0.910896, 89.019722, 0.000000 ) },
		{ Vector( -7810.416992, 5505.984375, 64.031250 ), Angle( 0.910896, 89.019722, 0.000000 ) },
		{ Vector( -7959.379395, 5508.535156, 64.031250 ), Angle( 0.991196, 89.421242, 0.000000 ) },
		{ Vector( -8147.642578, 5510.434570, 64.031250 ), Angle( 0.991196, 89.421242, 0.000000 ) },
		{ Vector( -8278.915039, 5511.757813, 64.031250 ), Angle( 0.991196, 89.421242, 0.000000 ) },
		{ Vector( -8435.704102, 5513.338867, 64.031250 ), Angle( 0.991196, 89.421242, 0.000000 ) },
		{ Vector( -8623.233398, 5515.231445, 64.031250 ), Angle( 0.991196, 89.421242, 0.000000 ) },
		{ Vector( -8794.698242, 5516.959961, 64.031250 ), Angle( 0.991196, 89.421242, 0.000000 ) },
	}
end

if SERVER then
	GM.Config.SSParkingZone = {
		Min = Vector( -6261.089844, -8126.217285, -151.764221 ),
		Max = Vector( -5246.113281, -7164.657715, 585.016541 ),
	}

	GM.Config.SSCarSpawns = {
		{ Vector( -5384.038086, -7657.681152, 64.031250 ), Angle( 0.910896, 90, 0.000000 ) },
		{ Vector( -5390.749512, -7827.928223, 64.031250 ), Angle( 0.910896, 90, 0.000000 ) },
	}
end

--[[ Mail Config ]]--
if SERVER then
	GM.Config.MailParkingZone = GM.Config.SalesParkingZone
	GM.Config.MailCarSpawns = GM.Config.SalesCarSpawns
end

GM.Config.MailDepotPos = Vector( -8743.177734, 7015.951660, 64.031250 )
GM.Config.MailPoints = {
	{ Pos = Vector('8414.269531 9146.810547 -1823.999878'), 	Name = "Subs Large 3", MinPrice = 20, MaxPrice = 60 },
	{ Pos = Vector('6250.940918 9148.338867 -1824.000000'), 	Name = "Subs Large 2", MinPrice = 20, MaxPrice = 60 },
	{ Pos = Vector('6422.318359 7860.912598 -1824.000122'), 	Name = "Subs Large 1", MinPrice = 20, MaxPrice = 60 },
	{ Pos = Vector('6890.182617 7898.719238 -1824.000000'), 	Name = "Subs Large 4", MinPrice = 20, MaxPrice = 60 },
	{ Pos = Vector('10800.728516 -3798.763916 -1728.000000'), 	Name = "Farmhouse 1", MinPrice = 20, MaxPrice = 60 },
	{ Pos = Vector('12786.304688 -10729.465820 -1728.000000'), 	Name = "Farmhouse 2", MinPrice = 20, MaxPrice = 60 },
	{ Pos = Vector('12788.152344 -11194.166016 -1728.000000'), 	Name = "Town House 3", MinPrice = 20, MaxPrice = 60 },
	{ Pos = Vector('12786.470703 -12332.996094 -1728.000000'), 	Name = "Town House 4", MinPrice = 20, MaxPrice = 60 },
	{ Pos = Vector('12790.519531 -13359.156250 -1728.000000'), 	Name = "Town House 5", MinPrice = 20, MaxPrice = 60 },
	{ Pos = Vector('10287.729492 -12130.004883 -1718.968750'), 	Name = "Bookstore", MinPrice = 20, MaxPrice = 60 },
	{ Pos = Vector('9671.493164 -12129.656250 -1718.968750'), 	Name = "Small Store 1", MinPrice = 20, MaxPrice = 60 },
	{ Pos = Vector('10742.080078 -12129.436523 -1718.968750'), 	Name = "Small Store 2", MinPrice = 20, MaxPrice = 60 },
	{ Pos = Vector('-2313.553223 1119.329590 80.031250'), 		Name = "Hospital", MinPrice = 20, MaxPrice = 60 },
	{ Pos = Vector('-3382.419189 3027.522705 76.031250'), 		Name = "Midas Repair Center", MinPrice = 20, MaxPrice = 60 },
	{ Pos = Vector('55.697128 2947.293213 76.031250'), 			Name = "Burger King", MinPrice = 20, MaxPrice = 60 },
	{ Pos = Vector('-2061.637695 -1068.993530 76.031250'), 		Name = "Hardware Store", MinPrice = 20, MaxPrice = 60 },
	{ Pos = Vector('-2367.445313 -2105.541016 76.031250'), 		Name = "Electronics Store", MinPrice = 20, MaxPrice = 60 },
	{ Pos = Vector('-2431.994629 -2450.214355 76.031250'), 		Name = "Evocity Clothing Store", MinPrice = 20, MaxPrice = 60 },
	{ Pos = Vector('2670.672607 -1743.491333 76.000008'), 		Name = "Car Dealership", MinPrice = 20, MaxPrice = 60 },
	{ Pos = Vector('3561.345215 -3026.221924 76.031250'), 		Name = "City Store", MinPrice = 20, MaxPrice = 60 },
	{ Pos = Vector('3544.175293 -2758.862793 76.031250'), 		Name = "City Apartment Market 103", MinPrice = 20, MaxPrice = 60 },
	{ Pos = Vector('3544.080566 -1319.191772 76.031250'), 		Name = "The Amber Room", MinPrice = 20, MaxPrice = 60 },
	{ Pos = Vector('668.994141 -1046.214722 76.031242'), 		Name = "City Realtor", MinPrice = 20, MaxPrice = 60 },
	{ Pos = Vector('3699.693848 506.647614 76.031250'), 		Name = "J&M Glass Co", MinPrice = 20, MaxPrice = 60 },
	{ Pos = Vector('3697.659424 1158.928955 76.031250'), 		Name = "City Resturant", MinPrice = 20, MaxPrice = 60 },
	{ Pos = Vector('3691.817871 1947.983643 76.031250'), 		Name = "Nathan's Drugs", MinPrice = 20, MaxPrice = 60 },
	{ Pos = Vector('2921.989014 2241.553711 76.031250'), 		Name = "Taxi HQ", MinPrice = 20, MaxPrice = 60 },
	{ Pos = Vector('1774.430542 -113.128632 140.031250'), 		Name = "Bank Of America", MinPrice = 20, MaxPrice = 60 },
	{ Pos = Vector('955.132263 916.679993 76.031235'), 			Name = "Skyscraper", MinPrice = 20, MaxPrice = 60 },
	{ Pos = Vector('-591.041321 -1394.952881 76.031250'), 		Name = "City Hall", MinPrice = 20, MaxPrice = 60 },
	{ Pos = Vector('7873.277344 6018.398926 70.031250'), 		Name = "Club Foods", MinPrice = 20, MaxPrice = 60 },
	{ Pos = Vector('4895.411621 12998.468750 68.000000'), 		Name = "Fire Station", MinPrice = 20, MaxPrice = 60 },
	{ Pos = Vector('-334.648987 11651.103516 -127.968765'), 	Name = "Old River House", MinPrice = 20, MaxPrice = 60 },
	{ Pos = Vector('-4519.119629 13615.670898 196.031250'), 	Name = "Warehouse 1", MinPrice = 20, MaxPrice = 60 },
	{ Pos = Vector('-3033.803955 13504.715820 197.000000'), 	Name = "Factory", MinPrice = 20, MaxPrice = 60 },
	{ Pos = Vector('-7354.874023 1376.570679 140.031250'), 		Name = "Gas Station", MinPrice = 20, MaxPrice = 60 },
}

--[[ Bus Config ]]--
if SERVER then
	GM.Config.BusParkingZone = GM.Config.SalesParkingZone
	GM.Config.BusCarSpawns = GM.Config.SalesCarSpawns
end

--[[ Vault bag spawn locations ]]--
if SERVER then

	GM.Config.BagLocations = {}

end


--[[ Fire Station Button and Vault ]]
if SERVER then

	hook.Add("InitPostEntity","SpawnFireShit",function()
		
		local vault = ents.Create( "ent_vault" )
		
		vault:SetPos( Vector( -4916, -5708, 8 ) )
		vault:SetAngles( Angle( 0, 180.000, 0.000 ) )
		vault:SetModel("models/custom/vault.mdl")
		vault:Spawn()
		vault.LastRobbery = CurTime() - (60*60)
		vault.BlockPhysGun = true
		vault.BagLocations = GAMEMODE.Config.BagLocations
		local phys = vault:GetPhysicsObject()
		if IsValid( phys ) then
			phys:EnableMotion( false )
		end
		
		
		local btn = ents.Create( "ent_button" )
		btn:SetModel( "models/maxofs2d/button_05.mdl" )
		btn:SetIsToggle( true )
		btn:SetPos( Vector( 1824.437500, 4315.250000, 597.250000 ) )
		btn:SetAngles( Angle( 90.000, 180.000, 180.000 ) )
		btn:Spawn()
		btn.IsMapProp = true
		local phys = btn:GetPhysicsObject()
		if IsValid( phys ) then
			phys:EnableMotion( false )
		end	
		btn:SetLabel( "Enable/Disable Alarm" )
		btn.BlockPhysGun = true
		btn:SetCanUseCallback( function( entBtn, pPlayer )
			return GAMEMODE.Jobs:GetPlayerJobID( pPlayer ) == JOB_FIREFIGHTER
		end )	

		function btn:Toggle( bEnable, pPlayer )

			for _, pos in pairs( GAMEMODE.Config.FireStationGarageDoors ) do
				for _, ent in pairs( ents.FindInSphere( pos , 3 ) ) do
					if ent:GetClass() == "func_door_rotating" then
						ent:Fire("open", "", .6)
						ent:Fire("setanimation", "open", .6)
					end
				end
			end

			self:EmitSound( "santosrp/fire_station.wav" )
		end
		
		

	end)

end

if SERVER then

	timer.Create( "DoorRefresh", 300, 0, function()

		for k, v in pairs( ents.GetAll() ) do

			if IsValid( v ) and GAMEMODE.Util:IsDoor( v ) then

				v:SetHealth( 999999999 )

			end

		end

	end )

end
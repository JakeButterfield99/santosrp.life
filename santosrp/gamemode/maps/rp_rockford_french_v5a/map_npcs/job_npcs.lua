--[[
	Name: job_npcs.lua
	For: TalosLife
	By: TalosLife
]]--

GAMEMODE.Map:RegisterNPCSpawn{
	UID = "cop_job",
	pos = { Vector( -8738.413086, -5587.968750, 8 ) },
	angs = { Angle( 0, -90, 0 ) },
}

GAMEMODE.Map:RegisterNPCSpawn{
	UID = "cop_spawn_car",
	pos = { Vector( -7624.612793, -5608.634766, 20 ) },
	angs = { Angle( 0, -180, 0 ) },
}

GAMEMODE.Map:RegisterNPCSpawn{
	UID = "cop_jail_warden",
	pos = { Vector( -8815.063477, -5582.154297, 8 ) },
	angs = { Angle( 0, -90, 0 ) },
}

GAMEMODE.Map:RegisterNPCSpawn{
	UID = "ems_spawn",
	pos = { Vector( -380.649261, 7984.721680, 613 ) },
	angs = { Angle( 0, 90, 0 ) },
}

--[[
GAMEMODE.Map:RegisterNPCSpawn{
	UID = "fire_chief",
	pos = { Vector( 1840.031250, 4144.793457, 536 ) },
	angs = { Angle( 0, 0, 0 ) },
}

GAMEMODE.Map:RegisterNPCSpawn{
	UID = "law_jobs",
	pos = { Vector( -5585.385254, -5448.031250, 8 ) },
	angs = { Angle( 0, 90, 0 ) },
}
]]--

GAMEMODE.Map:RegisterNPCSpawn{
	UID = "mayor_jobs",
	pos = { Vector( -4778.960449, -8961.394531, 64 ) },
	angs = { Angle( 0, 0, 0 ) },
}

GAMEMODE.Map:RegisterNPCSpawn{
	UID = "taxi_spawn",
	pos = { Vector( 1413.845947, 2980.418945, 584 ) },
	angs = { Angle( 0, 0, 0 ) },
}

GAMEMODE.Map:RegisterNPCSpawn{
	UID = "tow_spawn",
	pos = { Vector( -7386.017578, 776.326599, 1 ) },
	angs = { Angle( 0, 0, 0 ) },
}

GAMEMODE.Map:RegisterNPCSpawn{
	UID = "mail_spawn",
	pos = { Vector( -6985.473145, 5778.034180, 8 ) },
	angs = { Angle( 0, -180, 0 ) },
}

GAMEMODE.Map:RegisterNPCSpawn{
	UID = "sales_truck_spawn",
	pos = { Vector( -6984.209961, 5659.240234, 8 ) },
	angs = { Angle( 0, -180, 0 ) },
}

GAMEMODE.Map:RegisterNPCSpawn{
	UID = "bus_spawn",
	pos = { Vector( -6984.209961, 5976.665039, 8 ) },
	angs = { Angle( 0, -180, 0 ) },
}

GAMEMODE.Map:RegisterNPCSpawn{
	UID = "ss_spawn_car",
	pos = { Vector( -5214.861816, -7606.968750, 25 ) },
	angs = { Angle( 3.641090, 40.881596, 0.000000 ) },
}
local Prop = {}
Prop.Name = "Ibis Red | Room 0"
Prop.Cat = "Apartments"
Prop.Price = 800
Prop.Doors = {
    Vector(-1086.053101, -5490.706055, 143.031250),
    Vector(-722.158630, -5260.665527, 143.031250),
    Vector(-736.485596, -5387.444336, 143.031250),
    Vector(-637.490967, -5333.708008, 143.031250),
}

GM.Property:Register( Prop )
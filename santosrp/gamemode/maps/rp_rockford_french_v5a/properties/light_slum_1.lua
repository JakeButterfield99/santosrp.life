local Prop = {}
Prop.Name = "Light Slum | Room 1"
Prop.Cat = "Apartments"
Prop.Price = 800
Prop.Doors = {
	Vector( -890.348694, -3352.468018, 64.031250 ),
	Vector( -962.381348, -3048.219238, 64.031250 ),
	Vector( -887.250916, -2960.137939, 64.031250 ),
}

GM.Property:Register( Prop )
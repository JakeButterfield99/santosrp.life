local Prop = {}
Prop.Name = "Dark Slum | Room 1"
Prop.Cat = "Apartments"
Prop.Price = 800
Prop.Doors = {
	Vector( -1858.028320, -3354.735596, 64.031250 ),
	Vector( -1943.481445, -3049.650635, 64.031250 ),
	Vector( -1867.570557, -2962.329834, 64.031250 ),
}

GM.Property:Register( Prop )
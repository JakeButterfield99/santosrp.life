local Prop = {}
Prop.Name = "Ibis Blue | Room 4"
Prop.Cat = "Apartments"
Prop.Price = 800
Prop.Doors = {
    Vector(707.226501, -6562.645508, 416.031250),
    Vector(1065.147949, -6660.872559, 416.031250),
    Vector(1059.776489, -6786.381348, 416.031250),
    Vector(1154.905518, -6726.431641, 416.031250),
}

GM.Property:Register( Prop )
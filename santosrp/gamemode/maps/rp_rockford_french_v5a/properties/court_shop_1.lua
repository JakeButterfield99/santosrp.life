local Prop = {}
Prop.Name = "Court Shop 1"
Prop.Cat = "Stores"
Prop.Price = 800
Prop.Doors = {
	Vector( -6904.968750, -7547.342773, 72.031250 ),
	Vector( -6849.031250, -7547.828125, 72.031250 ),
}

GM.Property:Register( Prop )
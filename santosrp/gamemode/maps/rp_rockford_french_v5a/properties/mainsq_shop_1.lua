local Prop = {}
Prop.Name = "Main Square | Hardware"
Prop.Government = true
Prop.Doors = {
	Vector( -5220.031250, -3449.020996, 72.031250 ),
	Vector( -5272.941895, -3452.519043, 72.031250 ),
}

GM.Property:Register( Prop )
local Prop = {}
Prop.Name = "Gas Station"
Prop.Government = true
Prop.Doors = {
	Vector( -14274.420898, 2635.605957, 456.031250 ),
	Vector( -14277.515625, 2669.211914, 456.031250 ),
	Vector( 1900.349854, 2209.005127, 608.031250 ),
	Vector( 1900.005493, 2241.858398, 608.031250 ),
}

GM.Property:Register( Prop )
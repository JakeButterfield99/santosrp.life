local Prop = {}
Prop.Name = "Cosmos 195"
Prop.Cat = "Apartments"
Prop.Price = 800
Prop.Doors = {
	Vector( -8666.906250, -3227.961182, 72.031250 ),
	Vector( -8666.996094, -3176.567383, 72.031250 ),
}

GM.Property:Register( Prop )
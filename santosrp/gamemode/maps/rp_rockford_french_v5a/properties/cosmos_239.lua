local Prop = {}
Prop.Name = "Cosmos 239"
Prop.Cat = "Apartments"
Prop.Price = 800
Prop.Doors = {
	Vector( -8666.337891, 1131.434448, 72.031250 ),
	Vector( -8667.075195, 1172.035645, 72.031250 ),
	Vector( -8035.408203, 717.396912, 72.031250 ),
}

GM.Property:Register( Prop )
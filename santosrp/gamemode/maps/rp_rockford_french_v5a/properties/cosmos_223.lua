local Prop = {}
Prop.Name = "Cosmos 223"
Prop.Cat = "Apartments"
Prop.Price = 800
Prop.Doors = {
	Vector( -8665.596680, 25.088039, 72.031250 ),
	Vector( -8665.844727, 75.748878, 72.031250 ),
	Vector( -7964.773438, 411.877869, 72.031250 ),
	Vector( -7779.348145, 343.899658, 72.031250 ),
}

GM.Property:Register( Prop )
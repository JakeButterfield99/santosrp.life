local Prop = {}
Prop.Name = "Ibis Red | Room 3"
Prop.Cat = "Apartments"
Prop.Price = 800
Prop.Doors = {
    Vector(-1348.173828, -5490.222168, 415.031250),
    Vector(-1627.542358, -5763.676270, 415.423279),
    Vector(-1698.523804, -5261.093750, 415.031250),
    Vector(-1794.967651, -5320.784668, 415.031250),
    Vector(-1705.438110, -5388.166992, 415.031250),
}

GM.Property:Register( Prop )
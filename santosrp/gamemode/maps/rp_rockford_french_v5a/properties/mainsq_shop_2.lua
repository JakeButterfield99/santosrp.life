local Prop = {}
Prop.Name = "Main Square | Furniture"
Prop.Government = true
Prop.Doors = {
	Vector( -4891.240723, -3452.340332, 72.031250 ),
	Vector( -4843.374512, -3452.099365, 72.031250 ),
}

GM.Property:Register( Prop )
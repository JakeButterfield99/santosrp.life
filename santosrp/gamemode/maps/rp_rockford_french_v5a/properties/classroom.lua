local Prop = {}
Prop.Name = "Mall Classroom"
Prop.Cat = "Stores"
Prop.Price = 800
Prop.Doors = {
	Vector ( -1468.520264, 1222.098877, 600.031250 ),
	Vector ( -1512.398071, 1218.786011, 600.031250 ),
}

GM.Property:Register( Prop )
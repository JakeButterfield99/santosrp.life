local Prop = {}
Prop.Name = "Car Dealer"
Prop.Government = true
Prop.Doors = {
	Vector( -4274.036621, -1341.022461, 64.031250 ),
	Vector( -4226.960449, -1339.675903, 64.031250 ),
	Vector( -5378.463379, -789.440796, 64.031250 ),
	Vector( -5773.613281, -739.142395, 64.031250 ),
	Vector( -6189.906250, -739.745789, 64.031250 ),
	Vector( -6286.909668, -379.244965, 69.364586 ),
}

GM.Property:Register( Prop )
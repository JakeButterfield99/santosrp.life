local Prop = {}
Prop.Name = "Bazzar Market 9"
Prop.Cat = "Stores"
Prop.Price = 50
Prop.Doors = {
	Vector( -11245, -8688.5, 347 ),
	Vector( -11187, -8455.5, 347 ),
}

GM.Property:Register( Prop )
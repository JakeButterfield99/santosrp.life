local Prop = {}
Prop.Name = "Bazzar Market 4"
Prop.Cat = "Stores"
Prop.Price = 50
Prop.Doors = {
	Vector( -12205, -8688.5, 347 ),
	Vector( -12147, -8455.5, 347 ),
}

GM.Property:Register( Prop )
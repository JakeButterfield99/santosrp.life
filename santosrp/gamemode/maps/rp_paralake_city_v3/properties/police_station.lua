local Prop = {}
Prop.Name = "Police Station"
Prop.Government = true
Prop.Doors = {
	{ Pos = Vector( -9115, 10639, 343 ), Locked = true },
	{ Pos = Vector( -9203, 10872.5, 347 ), Locked = true },
	{ Pos = Vector( -8941, 10024.5, 343 ), Locked = true },
	{ Pos = Vector( -8899, 10024.5, 343 ), Locked = true },
	{ Pos = Vector( -9343, 10131, 343 ), Locked = true },
	{ Pos = Vector( -9261, 10559.5, 343 ), Locked = true },
	{ Pos = Vector( -9567.5, 9931, 231 ), Locked = true },
	{ Pos = Vector( -9403, 10479.5, 183 ), Locked = true },
	{ Pos = Vector( -9403, 10352.5, 183 ), Locked = true },
	{ Pos = Vector( -9103.5, 10393, 183 ), Locked = true },
	{ Pos = Vector( -9301, 10943.5, 223 ), Locked = true },
	{ Pos = Vector( -9301, 10943.5, 223 ), Locked = true },
	{ Pos = Vector( -8563, 10008.5, -348 ), Locked = true },
	{ Pos = Vector( -8563, 9300.5, -347.78125 ), Locked = true },
	{ Pos = Vector( -8935.5, 10087, -503 ), Locked = true },
	{ Pos = Vector( -9643.5, 10087, -502.78125 ), Locked = true },
}

GM.Property:Register( Prop )
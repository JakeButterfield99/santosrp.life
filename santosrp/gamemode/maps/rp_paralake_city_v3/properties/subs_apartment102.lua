local Prop = {}
Prop.Name = "Subs Apartment 102"
Prop.Cat = "Apartments"
Prop.Price = 100
Prop.Doors = {
	Vector( -3469, 9537, 566 ),
	Vector( -3515, 9191, 566 ),
	Vector( -3257, 9413, 566 ),
	Vector( -3257, 9229, 566 ),
}

GM.Property:Register( Prop )
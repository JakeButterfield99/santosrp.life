--[[
	Name: job_npcs.lua
	For: TalosLife
	By: TalosLife
]]--

GAMEMODE.Map:RegisterNPCSpawn{
	UID = "cop_job",
	pos = { Vector( -8933.183594, 10603.702148, 290.031250 ) },
	angs = { Angle( 0, 90, 0 ) },
}

GAMEMODE.Map:RegisterNPCSpawn{
	UID = "cop_spawn_car",
	pos = { Vector( -9584.031250, 10351.968750, 178.031250 ) },
	angs = { Angle( 0, -125, 0 ) },
}

GAMEMODE.Map:RegisterNPCSpawn{
	UID = "cop_jail_warden",
	pos = { Vector( -8983.859375, 10601.818359, 288.031250 ) },
	angs = { Angle( 0, 90, 0 ) },
}

GAMEMODE.Map:RegisterNPCSpawn{
	UID = "ems_spawn",
	pos = { Vector( -7066.561035, 6536.289551, 295.031250 ) },
	angs = { Angle( 0, 90, 0 ) },
}

GAMEMODE.Map:RegisterNPCSpawn{
	UID = "fire_chief",
	pos = { Vector( -13386.509766, 12784.031250, 80.031250 ) },
	angs = { Angle( 0, -90, 0 ) },
}

GAMEMODE.Map:RegisterNPCSpawn{
	UID = "taxi_spawn",
	pos = { Vector( -8071.846191, 1859.666016, 356.281250 ) },
	angs = { Angle( 0, 0, 0 ) },
}

GAMEMODE.Map:RegisterNPCSpawn{
	UID = "tow_spawn",
	pos = { Vector( -11136.031250, 10073.039063, 128.031250 ) },
	angs = { Angle( 0, -180, 0 ) },
}

GAMEMODE.Map:RegisterNPCSpawn{
	UID = "mail_spawn",
	pos = { Vector( -5797.379883, -7130.890625, 0.031250 ) },
	angs = { Angle( 0, -180, 0 ) },
}

GAMEMODE.Map:RegisterNPCSpawn{
	UID = "sales_truck_spawn",
	pos = { Vector( -11055.968750, -7872.283691, 356.031250 ) },
	angs = { Angle( 0, 0, 0 ) },
}

GAMEMODE.Map:RegisterNPCSpawn{
	UID = "bus_spawn",
	pos = { Vector( -5797.379883, -7177.979980, 0.031250 ) },
	angs = { Angle( 0, -180, 0 ) },
}

GAMEMODE.Map:RegisterNPCSpawn{
	UID = "mayor_jobs",
	pos = { Vector( -5584.031250, 10268.044922, 843.00 ) },
	angs = { Angle( 0, 0, 0 ) },
}

GAMEMODE.Map:RegisterNPCSpawn{
	UID = "law_jobs",
	pos = { Vector( -5641.314453, 12498.246094, 843.00 ) },
	angs = { Angle( 0, 0, 0 ) },
}

GAMEMODE.Map:RegisterNPCSpawn{
	UID = "ss_spawn_car",
	pos = { Vector( -6401.704102, 9778.579102, 371.114563 ) },
	angs = { Angle( 0, -90, 0 ) },
}
local Prop = {}
Prop.Name = "Burger King"
Prop.Cat = "Stores"
Prop.Price = 800
Prop.Doors = {
	Vector( -10654.676758, 9919.360352, 160.031250 ),
	Vector( -10657.161133, 9848.424805, 160.031250 ),
	Vector( -10508.968750, 9553.075195, 160.031250 ),
}


GM.Property:Register( Prop )
local Prop = {}
Prop.Name = "Oak City Car Dealer"
Prop.Government = true
Prop.Doors = {
	Vector( -13987.107422, -409.916595, 160.031250 ),
	Vector( -13728.673828, -830.805237, 160.031250 ),
	Vector( -13727.430664, -886.238953, 160.031250 ),
}

GM.Property:Register( Prop )
local Prop = {}
Prop.Name = "Shady Apartment 3"
Prop.Cat = "Apartments"
Prop.Price = 800
Prop.Doors = {
	Vector( -2777.576904, 1616.459717, 160.031250 ),
	Vector( -2762.672852, 1936.047729, 160.031250 ),
}

GM.Property:Register( Prop )
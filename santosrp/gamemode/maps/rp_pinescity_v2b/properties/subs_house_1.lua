local Prop = {}
Prop.Name = "Subs House 1"
Prop.Cat = "House"
Prop.Price = 800
Prop.Doors = {
	Vector( 6754.748047, -10187.189453, 152.031250 ),
	Vector( 6317.254395, -10383.589844, 152.031250 ),
	Vector( 6489.997559, -10125.123047, 152.031250 ),
	Vector( 6318.321777, -9793.378906, 152.031250 ),
	Vector( 6610.956543, -9926.271484, 152.031250 ),
}


GM.Property:Register( Prop )
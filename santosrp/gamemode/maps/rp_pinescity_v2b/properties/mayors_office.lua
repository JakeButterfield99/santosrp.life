local Prop = {}
Prop.Name = "Oak City Mayor's Office"
Prop.Government = true
Prop.Doors = {
	Vector( -8053.221191, 12308.968750, 160.031250 ),
	Vector( -8050.352539, 12222.031250, 160.031250 ),
	Vector( -8531.710938, 12489.031250, 160.031250 ),
	Vector( -9477.089844, 12509.877930, 160.031250 ),
	Vector( -8417.122070, 12079.122070, 883.031250 ),
	Vector( -8437.968750, 12079.626953, 1073.031250 ),
}

GM.Property:Register( Prop )
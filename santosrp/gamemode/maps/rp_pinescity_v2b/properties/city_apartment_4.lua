local Prop = {}
Prop.Name = "City Apartment 4"
Prop.Cat = "Apartments"
Prop.Price = 800
Prop.Doors = {
	Vector( -11434.756836, 11866.031250, 600.031250 ),
	Vector( -11747.242188, 11986.031250, 600.031250 ),
	Vector( -11542.169922, 12347.968750, 600.031250 ),
	Vector( -11543.511719, 12527.968750, 600.031250 ),
}

GM.Property:Register( Prop )
local Prop = {}
Prop.Name = "Oak City Bank"
Prop.Government = true
Prop.Doors = {
	Vector( -9826.122070, 10890.046875, 160.031250 ),
	Vector( -9899.877930, 10890.154297, 160.031250 ),
	Vector( -9671.372070, 11116.122070, 160.031250 ),
	Vector( -9674.274414, 11219.877930, 160.031250 ),
	Vector( -9652.315430, 11472.189453, 160.031250 ),
	Vector( -9566.189453, 11554.917969, 160.031250 ),
	Vector( -9564.031250, 11682.383789, 160.031250 ),
}

GM.Property:Register( Prop )
local Prop = {}
Prop.Name = "Pine Hotel - Room 3"
Prop.Cat = "Apartments"
Prop.Price = 800
Prop.Doors = {
	Vector( -6874.031250, 6715.368164, 300.031250 ),
	Vector( -6662.582520, 6656.031250, 300.031250 ),
}

GM.Property:Register( Prop )
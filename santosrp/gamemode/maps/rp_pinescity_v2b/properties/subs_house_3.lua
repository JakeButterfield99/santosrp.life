local Prop = {}
Prop.Name = "Subs House 3"
Prop.Cat = "House"
Prop.Price = 800
Prop.Doors = {
	Vector( 6649.641602, -7864.208008, 152.031250 ),
	Vector( 6523.189941, -8419.884766, 152.031250 ),
	Vector( 6609.445801, -8638.003906, 152.031250 ),
	Vector( 6268.958984, -8169.681152, 152.031250 ),
	Vector( 6156.189941, -8223.755859, 152.031250 ),
	Vector( 6068.809570, -8159.687012, 152.031250 ),
}

GM.Property:Register( Prop )
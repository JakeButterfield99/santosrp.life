local Prop = {}
Prop.Name = "Shady Apartment 4"
Prop.Cat = "Apartments"
Prop.Price = 800
Prop.Doors = {
	Vector( -2777.020996, 1622.952148, 282.031250 ),
	Vector( -2760.617920, 1936.638672, 282.031250 ),
}

GM.Property:Register( Prop )
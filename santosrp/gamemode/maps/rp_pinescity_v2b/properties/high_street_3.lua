local Prop = {}
Prop.Name = "High Street 3"
Prop.Cat = "Stores"
Prop.Price = 800
Prop.Doors = {
	Vector( -6672.519531, 10974.957031, 160.611542 ),
	Vector( -6501.537109, 11367.452148, 160.031250 ),
	Vector( -6500.354980, 11510.052734, 160.031250 ),
}


GM.Property:Register( Prop )
local Prop = {}
Prop.Name = "Subs House 4"
Prop.Cat = "House"
Prop.Price = 800
Prop.Doors = {
	Vector( 9110.605469, -8268.346680, 152.031250 ),
	Vector( 9193.217773, -8143.219727, 152.031250 ),
	Vector( 9299.388672, -8086.606445, 152.031250 ),
	Vector( 9392.954102, -8149.860352, 152.031250 ),
	Vector( 8547.279297, -8150.598633, 152.031250 ),
	Vector( 8413.628906, -8233.935547, 152.031250 ),
}

GM.Property:Register( Prop )
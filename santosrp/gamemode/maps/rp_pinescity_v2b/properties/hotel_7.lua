local Prop = {}
Prop.Name = "Pine Hotel - Room 7"
Prop.Cat = "Apartments"
Prop.Price = 800
Prop.Doors = {
	Vector( -6107.810059, 6716.899414, 440.031250 ),
	Vector( -6319.109863, 6671.968750, 440.031250 ),
}

GM.Property:Register( Prop )
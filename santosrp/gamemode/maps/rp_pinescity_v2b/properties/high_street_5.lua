local Prop = {}
Prop.Name = "High Street 5"
Prop.Cat = "Stores"
Prop.Price = 800
Prop.Doors = {
	Vector( -10814.810547, 11507.520508, 160.031250 ),
	Vector( -10814.968750, 11373.818359, 160.031250 ),
	Vector( -10982.210938, 10963.031250, 160.031250 ),
}

GM.Property:Register( Prop )
local Prop = {}
Prop.Name = "Warehouse 1"
Prop.Cat = "Warehouse"
Prop.Price = 800
Prop.Doors = {
	Vector( -11666.031250, -871.446594, 160.031250 ),
	Vector( -11725.968750, -872.915222, 160.031250 ),
	Vector( -11182.476563, -1032.451416, 215.027039 ),
	Vector( -11183.169922, -1337.966309, 215.027039 ),
	Vector( -11182.554688, -1634.916748, 215.027039 ),
}

GM.Property:Register( Prop )
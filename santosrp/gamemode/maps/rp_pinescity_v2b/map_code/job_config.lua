--[[
	Name: job_config.lua
	For: TalosLife
	By: TalosLife
]]--

--[[ EMS Config ]]--
if SERVER then
	GM.Config.EMSParkingZone = {
		Min = Vector( -5730.968750, 3205.031250, 70 ),
		Max = Vector( -5250.090332, 3561.568604, 340 ),
	}

	GM.Config.EMSCarSpawns = {
		{ Vector( -5514.752930, 3466.162598, 105 ), Angle( 0, -90, 0 ) },
		{ Vector( -5514.752930, 3293.220703, 105 ), Angle( 0, -90, 0 ) },
	}
end

--[[ Fire Config ]]--
if SERVER then
	GM.Config.FireParkingZone = {
		Min = Vector( -3921.187012, -107.783905, 73.416283 ),
		Max = Vector( -2973.120605, 594.930725, 378.329071 ),
	}

	GM.Config.FireCarSpawns = {
		{ Vector( -3728.246094, 340.477325, 105 ), Angle( 0, 0, 0 ) },
		{ Vector( -3538.157959, 340.477325, 105 ), Angle( 0, 0, 0 ) },
		{ Vector( -3335.949219, 340.477325, 105 ), Angle( 0, 0, 0 ) },
		{ Vector( -3150.880615, 340.477325, 105 ), Angle( 0, 0, 0 ) },
	}
end

--[[ Cop Config ]]--
if SERVER then
	GM.Config.CopParkingZone = {
		Min = Vector( -7155.978516, 11721.579102, 88.510048 ),
		Max = Vector( -5786.602051, 12681.063477, 385.245300 ),
	}

	GM.Config.CopCarSpawns = {
		{ Vector( -5863.937988, 12530.708984, 105 ), Angle( 1.284778, 69, 0.000000 ) },
		{ Vector( -5922.448242, 12341.867188, 105 ), Angle( 1.284778, 69, 0.000000 ) },
		{ Vector( -5921.812988, 12167.859375, 105 ), Angle( 1.284778, 69, 0.000000 ) },
		{ Vector( -5925.766113, 11995.399414, 105 ), Angle( 1.284778, 69, 0.000000 ) },
	}
end

--[[ Tow Config ]]--
if SERVER then
	GM.Config.TowWelderZone = {
		Min = Vector( -12008.449219, -3112.076904, -19.723221 ),
		Max = Vector( -11035.304688, -2465.008301, 435.193665 ),
	}

	GM.Config.TowParkingZone = {
		Min = Vector( -12008.449219, -3112.076904, -19.723221 ),
		Max = Vector( -9582.896484, -1975.280884, 589.190063 ),
	}

	GM.Config.TowCarSpawns = {
		{ Vector( -10111.447266, -1959.640991, 105 ), Angle( 0, 90, 0 ) },
		{ Vector( -10109.156250, -2099.149658, 105 ), Angle( 0, 90, 0 ) },
	}
end

--[[ Taxi Config ]]--
if SERVER then
	GM.Config.TaxiParkingZone = {
		Min = Vector( -8985.670898, 11095.399414, 63.403347 ),
		Max = Vector( -8167.232422, 11824.780273, 401.593658 ),
	}

	GM.Config.TaxiCarSpawns = {
		{ Vector( -8758.116211, 11618.541992, 97 ), Angle( 0, -90, 0 ) },
		{ Vector( -8761.109375, 11452.723633, 97 ), Angle( 0, -90, 0 ) },
	}
end

--[[ Sales Config ]]--
if SERVER then
	GM.Config.SalesParkingZone = {
		Min = Vector( -2987.499756, 2483.591797, 51.366158 ),
		Max = Vector( -2162.160889, 3101.026855, 321.583496 ),
	}

	GM.Config.SalesCarSpawns = {
		{ Vector( -2423.108887, 2709.497070, 97 ), Angle( 0, 90, 0 ) },
		{ Vector( -2422.627686, 2888.234131, 97 ), Angle( 0, 90, 0 ) },
	}
end

--[[ Mail Config ]]--
if SERVER then
	GM.Config.MailParkingZone = {
		Min = Vector( -13597.852539, -1527.859985, 41.794079 ),
		Max = Vector( -12667.549805, -104.674820, 454.731415 ),
	}

	GM.Config.MailCarSpawns = {
		{ Vector( -12862.788086, -571.836121, 97 ), Angle( 0, 70, 0 ) },
		{ Vector( -12819.048828, -787.355164, 97 ), Angle( 0, 70, 0 ) },
	}
end

GM.Config.MailDepotPos = Vector( -11449.255859, -1709.462402, 226.549530 )
GM.Config.MailPoints = {
	{ Pos = Vector('-10828.177734 9892.712891 97'), Name = "Burger King", MinPrice = 20, MaxPrice = 60 },
	{ Pos = Vector('-9870.051758 10724.083008 97'), Name = "Bank", MinPrice = 20, MaxPrice = 60 },
	{ Pos = Vector('-5447.449707 6820.262695 105'), Name = "Hotel", MinPrice = 20, MaxPrice = 60 },
	{ Pos = Vector('-6262.724609 9094.059570 161'), Name = "CityHall/Court", MinPrice = 20, MaxPrice = 60 },
	{ Pos = Vector('-2929.106934 11650.267578 -349'), Name = "City Car Park", MinPrice = 20, MaxPrice = 60 },
	{ Pos = Vector('-5114.560059 11920.905273 129'), Name = "Police Station", MinPrice = 20, MaxPrice = 60 },
	{ Pos = Vector('-9079.518555 11228.175781 105'), Name = "City Gas Station", MinPrice = 20, MaxPrice = 60 },
	{ Pos = Vector('-8191.684082 12264.754883 105'), Name = "Mayor's Office", MinPrice = 20, MaxPrice = 60 },
	{ Pos = Vector('-13624.753906 -1094.864990 105'), Name = "Car Dealer", MinPrice = 20, MaxPrice = 60 },
	{ Pos = Vector('6348.312500 -7341.239746 105'), Name = "Outer Gas Station", MinPrice = 20, MaxPrice = 60 },
	{ Pos = Vector('8098.988281 -9621.608398 97'), Name = "Suburb House", MinPrice = 20, MaxPrice = 60 },
	{ Pos = Vector('13271.230469 -6979.770996 -142'), Name = "Highway Construction Site", MinPrice = 20, MaxPrice = 60 },
	{ Pos = Vector('10265.113281 10024.291016 97'), Name = "Electricity Shack", MinPrice = 20, MaxPrice = 60 },
	{ Pos = Vector('-5379.219238 2472.907471 105'), Name = "Hospital", MinPrice = 20, MaxPrice = 60 },
	{ Pos = Vector('-2204.722656 3505.962646 102'), Name = "Garage Number 2", MinPrice = 20, MaxPrice = 60 },
	{ Pos = Vector('-3052.213135 3821.767822 102'), Name = "Garage Number 6", MinPrice = 20, MaxPrice = 60 },
	{ Pos = Vector('-2575.978027 515.715454 105'), Name = "Fire Department", MinPrice = 20, MaxPrice = 60 },
}

--[[ Bus Config ]]--
if SERVER then
	GM.Config.BusParkingZone = GM.Config.TaxiParkingZone

	GM.Config.BusCarSpawns = {
		{ Vector( -8758.116211, 11618.541992, 97 ), Angle( 0, -90, 0 ) },
		{ Vector( -8761.109375, 11452.723633, 97 ), Angle( 0, -90, 0 ) },
	}
end
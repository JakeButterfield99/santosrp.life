--[[
	Name: shop_npcs.lua
	For: TalosLife
	By: TalosLife
]]--

GAMEMODE.Map:RegisterNPCSpawn{
	UID = "car_dealer",
	pos = { Vector( -14321.548828, -870.411743, 105 ) },
	angs = { Angle( 0, 0, 0 ) },
}

GAMEMODE.Map:RegisterNPCSpawn{
	UID = "car_spawn",
	pos = { Vector( -3124.282471, 11804.820313, -341 ) },
	angs = { Angle( 0, -180, 0 ) },
}

GAMEMODE.Map:RegisterNPCSpawn{
	UID = "property_buy",
	pos = { Vector( -9972.003906, 11213.540039, 105 ) },
	angs = { Angle( 0, -90, 0 ) },
}

// Inner City Gas Station

GAMEMODE.Map:RegisterNPCSpawn{
	UID = "gas_clerk",
	pos = { Vector( -9077.299805, 11352.874023, 105 ) },
	angs = { Angle( 0, -90, 0 ) },
}

GAMEMODE.Map:RegisterNPCSpawn{
	UID = "club_foods_clerk",
	pos = { Vector( -9197.838867, 10928.288086, 105 ) },
	angs = { Angle( 0, 90, 0 ) },
}

// Outer City Gas Station

GAMEMODE.Map:RegisterNPCSpawn{
	UID = "gas_clerk",
	pos = { Vector( 6208.020508, -7313.938477, 105 ) },
	angs = { Angle( 0, 0, 0 ) },
}

--[[GAMEMODE.Map:RegisterNPCSpawn{
	UID = "club_foods_clerk",
	pos = { Vector( 6592.239258, -7360.662109, 105 ) },
	angs = { Angle( 0, 180, 0 ) },
}]]

GAMEMODE.Map:RegisterNPCSpawn{
	UID = "electronics_clerk",
	pos = { Vector( -8766.441406, 8486.439453, 111 ) },
	angs = { Angle( 0, -130, 0 ) },
}

GAMEMODE.Map:RegisterNPCSpawn{
	UID = "hardware_clerk",
	pos = { Vector( -9181.402344, 8494.646484, 111 ) },
	angs = { Angle( 0, -45, 0 ) },
}

GAMEMODE.Map:RegisterNPCSpawn{
	UID = "clothing_clerk",
	pos = { Vector( -9175.510742, 8076.179688, 111 ) },
	angs = { Angle( 0, 45, 0 ) },
}

GAMEMODE.Map:RegisterNPCSpawn{
	UID = "book_clerk",
	pos = { Vector( -8767.556641, 8070.607910, 111 ) },
	angs = { Angle( 0, 130, 0 ) },
}

local randPos = table.Random( GAMEMODE.Config.DrugNPCPositions )
GAMEMODE.Map:RegisterNPCSpawn{
	UID = "drug_buyer",
	pos = { randPos[1] },
	angs = { randPos[2] },
}

GAMEMODE.Map:RegisterNPCSpawn{
	UID = "home_items_clerk",
	pos = { Vector( -9362.416016, 8073.689941, 111 ) },
	angs = { Angle( 0, 130, 0 ) },
}
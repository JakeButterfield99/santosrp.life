--[[
	Name: job_item_lockers.lua
	For: SantosRP
	By: Ultra
]]--

local MapProp = {}
MapProp.ID = "job_item_lockers"
MapProp.m_tblSpawn = {}
MapProp.m_tblEnts = {
	{ pos = Vector(-5546.968750, 2995.732178, 96), ang = Angle(0, -180, 0), model = "models/props_c17/lockers001a.mdl", job = "JOB_EMS" },
	{ pos = Vector(-8743.031250, 12269.786133, 1009), ang = Angle(0, 90, 0), model = "models/props_c17/lockers001a.mdl", job = "JOB_SSERVICE" },
	{ pos = Vector(-2866.084961, -71.982216, 96), ang = Angle(0, 90, 0), model = "models/props_c17/lockers001a.mdl", job = "JOB_FIREFIGHTER" },
	{ pos = Vector(-5626.968750, 12485.390625, 440), ang = Angle(0, 0, 0), model = "models/props_c17/lockers001a.mdl", job = "JOB_POLICE" },
}

function MapProp:CustomSpawn()
	for _, propData in pairs( self.m_tblEnts ) do
		local ent = ents.Create( "ent_job_item_locker" )
		ent:SetPos( propData.pos )
		ent:SetAngles( propData.ang )
		ent:SetCollisionGroup( COLLISION_GROUP_NONE )
		ent:SetMoveType( MOVETYPE_NONE )
		ent.IsMapProp = true
		ent.MapPropID = id
		ent:Spawn()
		ent:Activate()
		ent:SetModel( propData.model )
		ent:SetJobID( _G[propData.job] )

		local phys = ent:GetPhysicsObject()
		if IsValid( phys ) then
			phys:EnableMotion( false )
		end
	end
end

GAMEMODE.Map:RegisterMapProp( MapProp )
--[[
	Name: job_npcs.lua
	For: TalosLife
	By: TalosLife
]]--

GAMEMODE.Map:RegisterNPCSpawn{
	UID = "cop_job",
	pos = { Vector( -3.74, -2373, 468 ) },
	angs = { Angle( 0, 90, 0 ) },
}

GAMEMODE.Map:RegisterNPCSpawn{
	UID = "cop_spawn_car",
	pos = { Vector( 620, -1736, -179 ) },
	angs = { Angle( 0, -170, 0 ) },
}

GAMEMODE.Map:RegisterNPCSpawn{
	UID = "cop_jail_warden",
	pos = { Vector( -628.083618, -1687.595337, -427.968750 ) },
	angs = { Angle( 0, 0, 0 ) },
}

GAMEMODE.Map:RegisterNPCSpawn{
	UID = "ems_spawn",
	pos = { Vector( -2602, 943, 76 ) },
	angs = { Angle( 0, 0, 0 ) },
}

GAMEMODE.Map:RegisterNPCSpawn{
	UID = "fire_chief",
	pos = { Vector( 4522.081055, 13119.193359, 68 ) },
	angs = { Angle( 0, 100, 0 ) },
}

GAMEMODE.Map:RegisterNPCSpawn{
	UID = "mayor_jobs",
	pos = { Vector( 4.482907, -2432.087891, 3802.808350 ) },
	angs = { Angle( -1.420048, 89.179428, 0 ) },
}

GAMEMODE.Map:RegisterNPCSpawn{
	UID = "law_jobs",
	pos = { Vector( 0.576991, -2418.255127, 1758.509644 ) },
	angs = { Angle( -1.860168, 90.858963, 0 ) },
}

GAMEMODE.Map:RegisterNPCSpawn{
	UID = "taxi_spawn",
	pos = { Vector( 2569.688721, 2339.317139, 76 ) },
	angs = { Angle( 0, 77.5, 0 ) },
}

GAMEMODE.Map:RegisterNPCSpawn{
	UID = "tow_spawn",
	pos = { Vector( 7290.288086, 13040.113281, 72.1 ) },
	angs = { Angle( 0, 90, 0 ) },
}

GAMEMODE.Map:RegisterNPCSpawn{
	UID = "mail_spawn",
	pos = { Vector( 9804.208984, 4795.696289, -1823.968750 ) },
	angs = { Angle( 0, -90, 0 ) },
}

GAMEMODE.Map:RegisterNPCSpawn{
	UID = "sales_truck_spawn",
	pos = { Vector( 9615.060547, 4758.068848, -1823.968750 ) },
	angs = { Angle( 0, 0, 0 ) },
}

GAMEMODE.Map:RegisterNPCSpawn{
	UID = "bus_spawn",
	pos = { Vector( 9637.499023, 4301.975586, -1823 ) },
	angs = { Angle( 0, -90, 0 ) },
}

GAMEMODE.Map:RegisterNPCSpawn{
	UID = "ss_spawn_car",
	pos = { Vector( 684.821350, -1793.322998, -115.968750 ) },
	angs = { Angle( 0, -90, 0 ) },
}
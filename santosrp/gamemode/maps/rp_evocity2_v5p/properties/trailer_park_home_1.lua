local Prop = {}
Prop.Name = "Trailer Park Home 1"
Prop.Cat = "House"
Prop.Price = 820
Prop.Doors = {
	Vector( 10723, 10796, -1764 ),
	Vector( 10563, 10796, -1764 ),
}

GM.Property:Register( Prop )
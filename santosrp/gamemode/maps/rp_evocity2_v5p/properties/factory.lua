local Prop = {}
Prop.Name = "Factory"
Prop.Cat = "Warehouse"
Prop.Price = 4100
Prop.Doors = {
	Vector( -3221, 13320, 316 ),
	Vector( -3221, 13048, 316 ),
	Vector( -3221, 14008, 316 ),
	Vector( -3221, 14280, 316 ),
	Vector( -2108, 13956, 262.28100585938 ),
	Vector( -3112, 13436, 262.281006 ),
	Vector( -1301, 12985, 126.25 ),
	Vector( -2006.968750, 13956.174805, 188.031250 ),
}

GM.Property:Register( Prop )
local Prop = {}
Prop.Name = "Small Store 1"
Prop.Cat = "Stores"
Prop.Price = 500
Prop.Doors = {
	Vector( 9732, -12058, -1664.75 ),
	Vector( 9596, -11594, -1664.75 ),
}

GM.Property:Register( Prop )
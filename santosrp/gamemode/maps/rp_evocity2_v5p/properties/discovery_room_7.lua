local Prop = {}
Prop.Name = "Discovery Room 7"
Prop.Cat = "House"
Prop.Price = 340
Prop.Doors = {
	Vector( 87.615211, 253.531250, 908.031250 ),
	Vector( 89.994385, 10.031251, 908.031250 ),
	Vector( 163.531250, -170.703979, 908.031250 ),
}


GM.Property:Register( Prop )
local Prop = {}
Prop.Name = "MTL Building B1-B2"
Prop.Cat = "Warehouse"
Prop.Price = 500
Prop.Doors = {
	Vector( 10070.031250, 3781.603760, -1759.968750 ),
	Vector( 10246.031250, 3470.740479, -1759.968750 ),
	Vector( 9921.510742, 2948.031250, -1759.968750 ),
	Vector( 9921.515625, 3347.968750, -1759.968750 ),
}

GM.Property:Register( Prop )
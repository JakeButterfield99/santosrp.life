local Prop = {}
Prop.Name = "Barn 2"
Prop.Cat = "Warehouse"
Prop.Price = 2200
Prop.Doors = {
	Vector( 9587, -7553, -1617 ),
	Vector( 9403, -7553, -1617 ),
}

GM.Property:Register( Prop )
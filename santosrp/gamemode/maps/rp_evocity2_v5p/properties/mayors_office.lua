local Prop = {}
Prop.Name = "Mayor's Office"
Prop.Government = true
Prop.Doors = {
	Vector( -384, -1906, 3849.25 ),
	Vector( -384, -2000, 3849.25 ),
	Vector( 14, -1452, 3849.25 ),
	Vector( -126, -1452, 3849.25 ),
	{ Pos = Vector( 59, -1752, 3849 ), Locked = true },	
}

GM.Property:Register( Prop )
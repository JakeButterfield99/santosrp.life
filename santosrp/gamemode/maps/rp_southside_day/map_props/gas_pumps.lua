--[[
	Name: gas_pumps.lua
	For: TalosLife
	By: TalosLife
]]--

local MapProp = {}
MapProp.ID = "gas_pumps"
MapProp.m_tblSpawn = {}
MapProp.m_tblEnts = {
	{ pos = Vector(385.471283, 13223.758789, 128.345505), ang = Angle(0, -90, 0) },
	{ pos = Vector(576.947205, 13223.758789, 128.345505), ang = Angle(0, -90, 0) },
	{ pos = Vector(-6063.298340, 662.824951, -31.585510), ang = Angle(0, -90, 0) },
	{ pos = Vector(-5857.347168, 662.824951, -31.585510), ang = Angle(0, -90, 0) },
}

function MapProp:CustomSpawn()
	for _, propData in pairs( self.m_tblEnts ) do
		local ent = ents.Create( "ent_fuelpump" )
		ent:SetPos( propData.pos )
		ent:SetAngles( propData.ang )
		ent.IsMapProp = true
		ent:Spawn()
		ent:Activate()

		local phys = ent:GetPhysicsObject()
		if IsValid( phys ) then
			phys:EnableMotion( false )
		end
	end
end

GAMEMODE.Map:RegisterMapProp( MapProp )
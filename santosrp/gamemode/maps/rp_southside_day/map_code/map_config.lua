--[[
	Name: map_config.lua
	For: TalosLife
	By: TalosLife
]]--

// Used in the Inventory Map
GM.Config.MapDimensions = {
	TopLeft = { -14658, 14976 },
	BottomRight = { 15296, -15299 }
}

if CLIENT then

	/*
		AddWaypoint(id, image, name, color, scale, pos, ang)
		Images defined in sh_config.lua
	*/
	-- Southside map has waypoints
	GM.Config.Waypoints = {}
	--[[GM.Config.Waypoints = {
		{ "pd", "map_pd", "PD", color_white, 1.8, Vector(-8229, -5710, 0) },
		{ "hospital", "map_ems", "Hospital", color_white, 1.6, Vector(54, 8496, 0) },
		{ "cardealer", "map_cardealer", "Car Dealer", color_white, 1.8, Vector(-4447, -778, 0) },
		{ "bank", "map_bank", "Bank", color_white, 1.8, Vector(-5517, -5494, 0) },
		{ "towtruck", "map_tow", "Tow Truck", color_white, 1.5, Vector(-7280, 745, 0) },
		{ "shell1", "map_shell", "Shell", color_white, 1.6, Vector(2097, 2160, 0) },
		{ "shell2", "map_shell", "Shell", color_white, 1.6, Vector(-14067, 2708, 0) },
		{ "truck", "map_truck", "City Jobs", color_white, 1.6, Vector(-7070, 5832, 0) },
		{ "downtownshops", "map_shop", "Downtown Shops", color_white, 1.6, Vector(-4098, -3082, 0) },
		{ "supermarket", "map_shop", "Supermarket", color_white, 1.6, Vector(-2431, 3471, 0) },
	}]]

end

if SERVER then

	--[[ Car Dealer Settings ]]--
	GM.Config.CarSpawns = {
		{ Vector( 6647.286621, 6831.226074, 128.943771 ), Angle( 0.561904, -89.619385, 0.000000 ) },
		{ Vector( 6644.551758, 7038.452637, 128.913284 ), Angle( 0.561904, -89.619385, 0.000000 ) },
		{ Vector( 6641.908691, 7241.846680, 128.972031 ), Angle( 0.561904, -89.619385, 0.000000 ) },
		{ Vector( 7218.988770, 6830.798340, 129.253235 ), Angle( 0.722505, 88.816383, 0.000000 ) },
		{ Vector( 7219.859863, 7061.033691, 128.965195 ), Angle( 0.722505, 88.816383, 0.000000 ) },
		{ Vector( 7220.519043, 7241.746094, 128.959702 ), Angle( 0.722505, 88.816383, 0.000000 ) },
		{ Vector( 7221.158203, 7468.002930, 129.055374 ), Angle( 0.722505, 88.816383, 0.000000 ) },
	}
	GM.Config.CarGarageBBox = {
		Min = Vector( 6243.767090, 6673.690430, 59.101257 ),
		Max = Vector( 7494.330566, 8062.894043, 652.265259 ),
	}

	--[[ Robbery Settings ]]

	GM.Config.RobberyMoneyDrops = {
		Vector( -5907.828125, 1495.410034, 84.031250 ),
	}

	--[[ Jail Settings ]]--
	GM.Config.JailPositions = {
		Vector( 9017.859375, 8872.415039, 264.031250 ),
		Vector( 9358.276367, 8872.279297, 264.692383 ),
		Vector( 9030.180664, 9110.712891, 264.910645 ),
		Vector( 9334.658203, 9112.468750, 264.414490 ),
		Vector( 9331.160156, 9330.643555, 264.031250 ),
		Vector( 9002.663086, 9327.284180, 264.031250 ),
	}
	GM.Config.JailReleasePositions = {
		Vector( 8420.082031, 7722.363281, 264.031250 ),
		Vector( 8354.828125, 7721.341309, 264.031250 ),
		Vector( 8292.059570, 7720.143555, 264.031250 ),
	}

	--[[ NPC Drug Dealer Settings ]]--
	GM.Config.DrugNPCPositions = {
		{ Vector( -134.292404, 7501.472656, 192.031250 ), Angle( 2.717212, 50.849487, 0.000000 ) },
		{ Vector( -2031.394409, 5808.295898, 81.942047 ), Angle( 0, 0, 0 ) },
		{ Vector( 972.383606, 3484.893066, -31.968750 ), Angle( 0, 180, 0 ) },
		{ Vector( 7911.464844, 1779.200928, -20.771980 ), Angle( 0, 90, 0 ) },
	}

	--[[ Map Settings ]]--
	GM.Config.SpawnPoints = {
		Vector( 2626.628418, 4975.827637, 64.031250 ),
		Vector( 2628.279785, 5040.501465, 64.031250 ),
		Vector( 2629.051758, 5115.690918, 64.031250 ),
		Vector( 2629.777832, 5186.391602, 64.031250 ),
		Vector( 2738.384766, 5188.059082, 64.031250 ),
		Vector( 2739.740479, 5097.840820, 64.031250 ),
		Vector( 2738.845459, 5010.626953, 64.031250 ),
		Vector( 2738.131836, 4941.145020, 64.031250 ),
	}

	--[[ Business Settings ]]--
	GM.Config.BusinessSupplyDrops = {
		Vector( 7294.054199, -907.383118, -83.573059 ),
		Vector( 3764.817383, -1284.050171, -105.755714 ),
		Vector( -6537.866699, 1072.142822, -11.545404 ),
		Vector( -6800.049316, -3696.116455, -299.542511 ),
	}

	GM.Config.HaulageIncDrops = {
		Vector( -3375.413818, -2014.795410, -75.547569 ),
		Vector( -3220.583496, -2018.250488, -75.657928 ),
		Vector( -3083.911621, -2020.754761, -75.509033 ),
	}

	--[[ Register the car customs shop location ]]--
	GM.CarShop.m_tblGarage["rp_southside_day"] = {
		Doors = {
			["truckbay"] = { CarPos = Vector( -2166.544922, 6233.556152, 28.429131 ) },
			["carbay"] = { CarPos = Vector( -2120.006348, 6614.427734, 23.973124 ) },
		},
		BBox = {
			Min = Vector( 0, 0, 0 ),
			Max = Vector( 0, 0, 0 ),
		},
		PlayerSetPos = Vector(0, 0, 0),
	}

	--[[ Fire Spawner Settings ]]--
	GM.Config.AutoFiresEnabled = false
	GM.Config.AutoFireSpawnMinTime = 60 *15
	GM.Config.AutoFireSpawnMaxTime = 60 *60
	GM.Config.AutoFireSpawnPoints = {
		Vector( -7313.187988, 3557.926025, 72.031250 ),
		Vector( -1816.031250, -2984.031250, 185.031250 ),
		Vector( -13919.029297, 2747.661133, 456.031250 ),
		Vector( 11207.075195, -8345.997070, 388.031250 ),
		Vector( -8461.106445, -14172.270508, 208.02339 ),
		Vector( 245.495132, 1563.968750, 601.031250 ),
		Vector( 2243.959229, 2101.555908, 608.022583 ),
		Vector( -6544.031250, -8236.968750, 72.031250 ),
		Vector( -941.840942, -5719.105957, 279.031250 ),
		Vector( 8970.031250, 944.894043, 1632.031250 ),
		Vector( 10538.031250, 7759.105957, 1632.031250 ),
		Vector( -8963.466797, 8615.889648, 72.031250 ),
		Vector( -8954.574219, 8522.129883, 72.031250 ),
	}
end

--[[ Car Dealer Settings ]]--
GM.Config.CarPreviewModelPos = Vector( -6972.135742, 1614.068970, -28.978018 )
GM.Config.CarPreviewCamPos = Vector( -6849.399902, 1389.579224, 100.468185 )
GM.Config.CarPreviewCamAng = Angle( 22.564171, 119.711586, 0.000000 )
GM.Config.CarPreviewCamLen = 2

--[[ Chop Shop ]]--
GM.Config.ChopShop_ChopLocation = Vector( -3070.178955, -2374.548828, -31.968750 )

--[[ Weather & Day Night ]]--
GM.Config.Weather_SkyZPos = nil --Let the code figure the zpos out on this map
GM.Config.FogLightingEnabled = false --This will override the fog hiding the farz

GM.Config.UnwashedLenght = 60 -- Minutes
GM.Config.WashedWorth = 1000 -- Bag worth
GM.Config.PrivateBankDoors = {Vector(-5224.000000, -5815.000000, 62.281300),Vector(-5077.000000, -5548.000000, 62.281300),Vector(-5065.000000, -5204.000000, 62.281200)}
GM.Config.AlarmLocation = Vector(-5067.359863, -5720.408203, 108.781250)
GM.Config.BankCoolDown = 1 *60
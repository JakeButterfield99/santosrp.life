local Prop = {}
Prop.Name = "Motel 8"
Prop.Cat = "Hotels"
Prop.Price = 150
Prop.Doors = {
	Vector( -4300.000000, 1222.000000, 132.000000 ),
	Vector( -4297.968750, 1602.000000, 132.000000 ),
}

GM.Property:Register( Prop )
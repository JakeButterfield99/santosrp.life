local Prop = {}
Prop.Name = "Park Apartment C"
Prop.Cat = "Apartments"
Prop.Price = 200
Prop.Doors = {
	Vector( 1046.000000, 2644.000000, 220.000000 ),
	Vector( 956.000000, 2486.000000, 220.000000 ),
}

GM.Property:Register( Prop )
local Prop = {}
Prop.Name = "Motel 4"
Prop.Cat = "Hotels"
Prop.Price = 150
Prop.Doors = {
	Vector( -3916.000000, 1222.000000, -44.000000 ),
	Vector( -3913.968750, 1602.000000, -44.000000 ),
}

GM.Property:Register( Prop )
local Prop = {}
Prop.Name = "Hideout"
Prop.Cat = "Apartments"
Prop.Price = 350
Prop.Doors = {
	Vector( -3849.000000, 6022.000000, 100.000000 ),
	Vector( -3888.000000, 6304.000000, 160.000000 ),
	Vector( -4218.000000, 7160.000000, 180.000000 ),
}

GM.Property:Register( Prop )
local Prop = {}
Prop.Name = "Electronics Store"
Prop.Government = true
Prop.Doors = {
	Vector( -1009.989990, 11494.000000, 188.000000 ),
	Vector( -508.000000, 11558.000000, 188.000000 ),
	Vector( -332.000000, 11558.000000, 188.000000 ),
}

GM.Property:Register( Prop )
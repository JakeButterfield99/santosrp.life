local Prop = {}
Prop.Name = "City Office Floor 8"
Prop.Cat = "Offices"
Prop.Price = 300
Prop.Doors = {
	Vector( -1014.000000, 5074.000000, 1252.000000 ),
	Vector( -656.000000, 4856.000000, 1252.000000 ),
	Vector( -922.000000, 5420.000000, 1252.000000 ),
	Vector( -1418.000000, 5074.000000, 1252.000000 ),
	Vector( -1776.000000, 4856.000000, 1252.000000 ),
}


GM.Property:Register( Prop )
local Prop = {}
Prop.Name = "Car Dealer"
Prop.Government = true
Prop.Doors = {
	Vector( -7121.593750, 1355.000000, 20.000000 ),
	Vector( -7238.375000, 1355.000000, 20.000000 ),
}

GM.Property:Register( Prop )
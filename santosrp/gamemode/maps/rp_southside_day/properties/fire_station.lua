local Prop = {}
Prop.Name = "Fire Station"
Prop.Cat = "Warehouses"
Prop.Price = 500
Prop.Doors = {
	Vector( 9128.000000, 1791.000000, -3.000000 ),
	Vector( 9132.000000, 1590.125000, -52.000000 ),
	Vector( 9132.000000, 1481.843750, -52.000000 ),
	Vector( 9128.000000, 1288.000000, -3.000000 ),
}

GM.Property:Register( Prop )
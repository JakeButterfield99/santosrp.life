local Prop = {}
Prop.Name = "Townhouse 6"
Prop.Cat = "Houses"
Prop.Price = 300
Prop.Doors = {
	Vector( -6170.000000, 13516.000000, 212.000000 ),
	Vector( -6074.000000, 14092.000000, 212.000000 ),
	Vector( -6218.000000, 13964.000000, 212.000000 ),
	Vector( -6412.000000, 14102.000000, 212.000000 ),
	Vector( -6602.000000, 14060.000000, 212.000000 ),
	Vector( -6720.000000, 13648.000000, 220.000000 ),
}

GM.Property:Register( Prop )
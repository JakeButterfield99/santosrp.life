local Prop = {}
Prop.Name = "Townhouse 3"
Prop.Cat = "Houses"
Prop.Price = 300
Prop.Doors = {
	Vector( -2341.968750, 13452.000000, 220.000000 ),
	Vector( -2404.000000, 13818.000000, 220.250000 ),
	Vector( -2310.000000, 14028.000000, 220.250000 ),
	Vector( -2186.000000, 13756.000000, 220.250000 ),
	Vector( -2016.000000, 13376.000000, 252.000000 ),
	Vector( -2028.000000, 13774.000000, 220.250000 ),
}

GM.Property:Register( Prop )
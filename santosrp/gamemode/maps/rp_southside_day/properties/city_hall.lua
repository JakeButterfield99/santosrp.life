local Prop = {}
Prop.Name = "City Hall"
Prop.Government = true
Prop.Doors = {
	Vector( 3432.010010, 4998.140137, 56.000000 ),
	Vector( 3432.010010, 5113.859863, 56.000000 ),
	Vector( 3148.010010, 4890.000000, 56.000000 ),
	Vector( 3148.010010, 5222.000000, 56.000000 ),
	Vector( 3112.010010, 4998.140137, 280.000000 ),
	Vector( 3112.010010, 5113.859863, 280.000000 ),
}

GM.Property:Register( Prop )
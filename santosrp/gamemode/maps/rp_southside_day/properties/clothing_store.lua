local Prop = {}
Prop.Name = "Clothing Store"
Prop.Government = true
Prop.Doors = {
	Vector( -2136.000000, 10868.000000, 180.000000 ),
	Vector( -2024.000000, 10868.000000, 180.000000 ),
	Vector( -2362.000000, 10496.000000, 180.000000 ),
	Vector( -2249.989990, 10316.000000, 180.000000 ),
}

GM.Property:Register( Prop )
local Prop = {}
Prop.Name = "Haulage Inc."
Prop.Government = true
Prop.Doors = {
	Vector( -2960.000000, -1312.000000, 64.000000 ),
	Vector( -2940.000000, -1573.968750, -44.000000 ),
	Vector( -2960.000000, -1888.000000, 64.000000 ),
}

GM.Property:Register( Prop )
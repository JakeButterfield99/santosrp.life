local Prop = {}
Prop.Name = "City Store 1"
Prop.Cat = "Stores"
Prop.Price = 350
Prop.Doors = {
	Vector( 4740.000000, 4840.000000, 4.000000 ),
	Vector( 5324.000000, 4726.000000, 4.000000 ),
	Vector( 5603.968750, 5001.968750, 4.000000 ),
}

GM.Property:Register( Prop )
local Prop = {}
Prop.Name = "Gas Station"
Prop.Government = true
Prop.Doors = {
	Vector( -5926.000000, 1166.000000, 28.000000 ),
	Vector( -6038.000000, 1604.000000, 28.000000 ),
	Vector( -6388.000000, 1754.000000, 28.000000 ),
}

GM.Property:Register( Prop )
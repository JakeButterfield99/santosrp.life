local Prop = {}
Prop.Name = "City Store 2"
Prop.Cat = "Stores"
Prop.Price = 350
Prop.Doors = {
	Vector( 4740.000000, 5416.000000, 4.000000 ),
	Vector( 5324.000000, 5578.000000, 4.000000 ),
	Vector( 5603.968750, 5353.968750, 4.000000 ),
}

GM.Property:Register( Prop )
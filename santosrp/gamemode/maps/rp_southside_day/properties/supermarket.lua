local Prop = {}
Prop.Name = "Supermarket"
Prop.Government = true
Prop.Doors = {
	Vector( 618.000000, 13838.000000, 191.000000 ),
	Vector( 726.000000, 13838.000000, 191.000000 ),
	Vector( 348.000000, 14110.000000, 188.000000 ),
	Vector( 76.000000, 14086.000000, 188.000000 ),
	Vector( 181.869995, 14380.000000, 189.000000 ),
	Vector( 182.000000, 14632.000000, 189.000000 ),
}

GM.Property:Register( Prop )
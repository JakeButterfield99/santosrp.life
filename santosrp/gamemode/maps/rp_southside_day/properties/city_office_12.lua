local Prop = {}
Prop.Name = "City Office Floor 12"
Prop.Cat = "Offices"
Prop.Price = 300
Prop.Doors = {
	Vector( -1173.968750, 5148.000000, 1812.000000 ),
	Vector( -674.000000, 4901.000000, 1816.000000 ),
	Vector( -1510.000000, 5516.000000, 1812.500000 ),
}


GM.Property:Register( Prop )
local Prop = {}
Prop.Name = "Motel 2"
Prop.Cat = "Hotels"
Prop.Price = 150
Prop.Doors = {
	Vector( -4684.000000, 1222.000000, -44.000000 ),
	Vector( -4681.968750, 1602.000000, -44.000000 ),
}

GM.Property:Register( Prop )
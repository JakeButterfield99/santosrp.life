local Prop = {}
Prop.Name = "Townhouse 7"
Prop.Cat = "Houses"
Prop.Price = 300
Prop.Doors = {
	Vector( -3878.000000, 11378.000000, 220.000000 ),
	Vector( -3862.000000, 11084.000000, 220.000000 ),
	Vector( -4426.000000, 10964.000000, 220.000000 ),
	Vector( -4544.000000, 11328.000000, 220.000000 ),
	Vector( -4452.000000, 10694.000000, 220.000000 ),
	Vector( -4316.000000, 10746.000000, 220.000000 ),
}

GM.Property:Register( Prop )
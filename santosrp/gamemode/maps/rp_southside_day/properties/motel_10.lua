local Prop = {}
Prop.Name = "Motel 10"
Prop.Cat = "Hotels"
Prop.Price = 150
Prop.Doors = {
	Vector( -3913.968750, 1602.000000, 132.000000 ),
	Vector( -3198.000000, 838.000000, 132.000000 ),
}

GM.Property:Register( Prop )
local Prop = {}
Prop.Name = "City Apartment 7"
Prop.Cat = "Apartments"
Prop.Price = 250
Prop.Doors = {
	Vector( 1222.000000, 4820.000000, 612.000000 ),
	Vector( 1132.000000, 4534.000000, 612.000000 ),
}

GM.Property:Register( Prop )
local Prop = {}
Prop.Name = "City Apartment 6"
Prop.Cat = "Apartments"
Prop.Price = 250
Prop.Doors = {
	Vector( 1524.000000, 5114.000000, 444.000000 ),
	Vector( 1030.000000, 5244.000000, 444.000000 ),
}

GM.Property:Register( Prop )
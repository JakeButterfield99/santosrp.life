local Prop = {}
Prop.Name = "Motel 5"
Prop.Cat = "Hotels"
Prop.Price = 150
Prop.Doors = {
	Vector( -3578.000000, 836.000000, -44.000000 ),
	Vector( -3198.000000, 838.000000, -44.000000 ),
}

GM.Property:Register( Prop )
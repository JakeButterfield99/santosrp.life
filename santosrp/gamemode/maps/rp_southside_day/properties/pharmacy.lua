local Prop = {}
Prop.Name = "Pharmacy"
Prop.Cat = "Stores"
Prop.Price = 450
Prop.Doors = {
	Vector( -6504.000000, 2956.000000, 20.000000 ),
	Vector( -6662.000000, 3483.000000, 28.000000 ),
	Vector( -6564.000000, 3705.968750, 20.000000 ),
}

GM.Property:Register( Prop )
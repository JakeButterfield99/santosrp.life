--[[
	Name: job_npcs.lua
	For: TalosLife
	By: TalosLife
]]--

GAMEMODE.Map:RegisterNPCSpawn{
	UID = "cop_job",
	pos = { Vector( 7949.596191, 8046.784180, 200.031250 ) },
	angs = { Angle( 0, -90, 0 ) },
}

GAMEMODE.Map:RegisterNPCSpawn{
	UID = "cop_spawn_car",
	pos = { Vector( 8338.911133, 7983.226074, -119.968750 ) },
	angs = { Angle( 0, 140, 0 ) },
}

GAMEMODE.Map:RegisterNPCSpawn{
	UID = "cop_jail_warden",
	pos = { Vector( 8954.639648, 8139.221191, 200.031250 ) },
	angs = { Angle( 0, -110, 0 ) },
}

GAMEMODE.Map:RegisterNPCSpawn{
	UID = "ems_spawn",
	pos = { Vector( 7492.867188, 5420.280273, -55.968750 ) },
	angs = { Angle( 0, -90, 0 ) },
}

--[[
GAMEMODE.Map:RegisterNPCSpawn{
	UID = "fire_chief",
	pos = { Vector( 1840.031250, 4144.793457, 536 ) },
	angs = { Angle( 0, 0, 0 ) },
}

GAMEMODE.Map:RegisterNPCSpawn{
	UID = "law_jobs",
	pos = { Vector( -5585.385254, -5448.031250, 8 ) },
	angs = { Angle( 0, 90, 0 ) },
}
]]--

GAMEMODE.Map:RegisterNPCSpawn{
	UID = "mayor_jobs",
	pos = { Vector( 2512.968750, 4991.216309, 0.031250 ) },
	angs = { Angle( 0, 0, 0 ) },
}

GAMEMODE.Map:RegisterNPCSpawn{
	UID = "taxi_spawn",
	pos = { Vector( -344.670410, 13735.165039, 128.031250 ) },
	angs = { Angle( 0, -90, 0 ) },
}

GAMEMODE.Map:RegisterNPCSpawn{
	UID = "tow_spawn",
	pos = { Vector( 3702.238770, -2251.411621, -131.970367 ) },
	angs = { Angle( 0, 0, 0 ) },
}

GAMEMODE.Map:RegisterNPCSpawn{
	UID = "mail_spawn",
	pos = { Vector( -2889.451416, -2083.556641, -95.968750 ) },
	angs = { Angle( 0, 0, 0 ) },
}

--[[
GAMEMODE.Map:RegisterNPCSpawn{
	UID = "sales_truck_spawn",
	pos = { Vector( -6984.209961, 5659.240234, 8 ) },
	angs = { Angle( 0, -180, 0 ) },
}
]]

GAMEMODE.Map:RegisterNPCSpawn{
	UID = "bus_spawn",
	pos = { Vector( -1786.816040, -2109.961426, -95.968750 ) },
	angs = { Angle( 0, 180, 0 ) },
}

GAMEMODE.Map:RegisterNPCSpawn{
	UID = "ss_spawn_car",
	pos = { Vector( 3563.645020, 4337.878906, -55.968750 ) },
	angs = { Angle( 0, -90, 0 ) },
}
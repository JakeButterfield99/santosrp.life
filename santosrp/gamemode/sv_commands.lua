--[[
	Name: sv_commands.lua
	For: OakRoleplay
	By: Jake Butts
]]--

GM.Commands = (GAMEMODE or GM).Commands or {}

function GM.Commands:FindPlayer( sName )

	local tPlyList = {}

	for k, v in pairs( player.GetAll() ) do
		if v:Nick():lower():find( sName:lower() ) then
			table.insert( tPlyList , v )
		end
	end

	return tPlyList

end

function GM.Commands:AdminPrint( type, ply, vic )

	for k, v in pairs( player.GetAll() ) do

		if v:IsSuperAdmin() or (v:GetUserGroup() == "sa") then

			v:PrintMessage( HUD_PRINTCONSOLE , "OakRoleplay | "..ply:Nick().." has ran command "..type.." on "..vic:Nick() )

		end

	end

end

function GM.Commands:RevivePlayer( pPlayer , pAdmin )

	if pPlayer:IsUncon() then

		pPlayer:WakeUp()

	elseif pPlayer:IsRagdolled() then

		pPlayer:UnRagdoll()

	end

	GAMEMODE.PlayerDamage:HealPlayerLimbs( pPlayer )
	pPlayer:SetHealth( pPlayer:GetMaxHealth() )

	self:AdminPrint( "Revive", pAdmin, pPlayer )

end

function GM.Commands:HealPlayer( pPlayer , pAdmin )

	GAMEMODE.PlayerDamage:HealPlayerLimbs( pPlayer )
	pPlayer:SetHealth( pPlayer:GetMaxHealth() )

	self:AdminPrint( "Heal", pAdmin, pPlayer )

end

function GM.Commands:RepairCar( pPlayer , pAdmin )

	local car = pPlayer:GetVehicle()
	if !car then return end

	GAMEMODE.Cars:FixVehicleWheels( car )
	GAMEMODE.Cars:ResetCarHealth( car )

	self:AdminPrint( "Repair", pAdmin, pPlayer )

end

function GM.Commands:CanRunCommand( pPlayer )

	if pPlayer:IsSuperAdmin() then return true end
	if pPlayer:GetUserGroup() == "sa" then return true end

	return false

end

function GM.Commands:RunCommand( pPlayer , sText , pAdmin )

	local tPlyList = self:FindPlayer( pPlayer )
	local bReturn = false

	if !(#tPlyList > 0) then
		return
	end

	for _, v in pairs( tPlyList ) do

		if !IsValid( v ) then return end

		if sText == "!heal" then

			self:HealPlayer( v , pAdmin )
			bReturn = true

		elseif sText == "!revive" then

			self:RevivePlayer( v , pAdmin )
			bReturn = true

		elseif sText == "!repair" then

			self:RepairCar( v , pAdmin )
			bReturn = true

		end

	end

	return bReturn

end

hook.Add("PlayerSay", "OakRoleplayCommands", function( pPlayer, sText )

	if !GAMEMODE.Commands:CanRunCommand( pPlayer ) then return sText end

    local chatmsg = string.Explode( " " , sText )
    local prefix = string.sub( chatmsg[1], 1, 1 )
    local bReturn = false

    if prefix == "!" then

    	if chatmsg[2] then

    		bReturn = GAMEMODE.Commands:RunCommand( chatmsg[2] , chatmsg[1], pPlayer )

    	end

    end

    if bReturn then
    	return ""
    end

end)